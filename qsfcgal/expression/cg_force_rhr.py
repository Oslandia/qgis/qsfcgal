# QGIS
from qgis.core import QgsGeometry
from qgis.utils import qgsfunction

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


@qgsfunction(
    args="auto",
    group="SFCGAL",
    register=False,
)
def cg_force_rhr(value, feature, parent):
    """
    <style>
    span { color: red }

    </style>
    <h1>Description</h1>

    Forces the orientation of the vertices in a polygon to follow a Right-Hand-Rule, in
    which the area that is bounded by the polygon is to the right of the boundary. In
    particular, the exterior ring is orientated in a clockwise direction and the interior
    rings in a counter-clockwise direction.
    <br/><br/>
    <h1>Auguments</h1>
    <table>
        <tr><td><span>geometry</span></td><td>Polygon/MultiPolygon geometry</td></tr>
    </table>
    <br/><br/>
    <h1>Syntax</h1>
    cg_force_rhr(<span>@geometry</span>)
    <br/><br/>
    <h1>Examples</h1>
    <ul>
        <li>Input: 'MultiPolygon (((8.239 10.909,7.222 10.649,7.391 10.197,8.329 9.993,8.894 10.219,8.883 10.784,8.239 10.909)))'</li>
        <li>Output: 'MultiPolygon (((8.239 10.909, 8.883 10.784, 8.894 10.219, 8.329 9.993, 7.391 10.197, 7.222 10.649, 8.239 10.909)))'</li>
    </ul>
    """
    geom_input = QgsGeometry_to_SFCGAL(value)
    if not geom_input.geom_type in ["Polygon", "MultiPolygon"]:
        return None
    result_geom = geom_input.force_rhr()
    new_geom = SFCGAL_to_QgsGeometry(result_geom)
    return new_geom
