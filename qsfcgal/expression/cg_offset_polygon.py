# QGIS
from qgis.core import QgsGeometry
from qgis.utils import qgsfunction

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


@qgsfunction(
    args="auto",
    group="SFCGAL",
    register=False,
    # helpText=(
    #     "A convex hull is the smallest convex shape that encloses a given set of "
    #     "points in space. It's like the tightest rubber band stretched around a set "
    #     "of points, forming the smallest convex polygon or polyhedron that contains "
    #     "all the points. "
    #     "To enter, you need to provide point or multipoint geometry. "
    # ),
)
def cg_offset_polygon(value, distance, feature, parent):
    """
    <style>
    span { color: red }

    </style>
    <h1>Description</h1>

    The geometric operation 'offset_polygon' involves creating a contour parallel to an
    original polygon, at a specified distance, typically referred to as the offset distance
    or buffer. This operation takes as input any type of geometry (point, line, polygon)
    and outputs a Multipolygon.
    <br/><br/>
    <h1>Auguments</h1>
    <table>
        <tr><td><span>geometry</span></td><td>Any geometry</td></tr>
        <tr><td><span>distance</span></td><td>Float</td></tr>
    </table>
    <br/><br/>
    <h1>Syntax</h1>
    cg_offset_polygon(<span>@geometry</span>, 1)
    <br/><br/>
    <h1>Examples</h1>
    <ul>
        <li>input_geom = 'MultiPolygon (((2.82738681 0.88666942, 2.80207035 0.43097322, 1.02991845 0.62084663, 0.78941212 -0.60700147, 2.54890579 -0.63231792, 2.58688048 -1.29054577, 0.38434883 -1.16396349, 0.6881463 0.88666942, 2.82738681 0.88666942)))'</li>
        <li>distance =  1</li>
        <li>cg_offset_polygon(input_geom, distance) = 'MultiPolygon (((-0.60485481 -1.01741605, .... -0.17740661 1.38748691, -0.30105703 1.03321898, -0.47521755 -0.14235713, -0.60485481 -1.01741605)))'</li>
    </ul>
    """
    geom_input = QgsGeometry_to_SFCGAL(value)
    if distance == 0:
        return

    result_geom = geom_input.offset_polygon(distance)
    new_geom = SFCGAL_to_QgsGeometry(result_geom)
    return new_geom
