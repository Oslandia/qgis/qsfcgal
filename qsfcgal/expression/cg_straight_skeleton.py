# QGIS
from qgis.core import QgsGeometry
from qgis.utils import qgsfunction

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


@qgsfunction(
    args="auto",
    group="SFCGAL",
    register=False,
    helpText=(
        "In geometry, a straight skeleton is a method of representing a polygon "
        "by a topological skeleton. You need to provide a poygon geometry as input "
        "and a linestring geometry as output."
    ),
)
def cg_straight_skeleton(value, feature, parent):
    geom_input = QgsGeometry_to_SFCGAL(value)
    if not geom_input.geom_type in ["Polygon", "MultiPolygon"]:
        return None

    result_geom = geom_input.straight_skeleton()
    new_geom = SFCGAL_to_QgsGeometry(result_geom)
    return new_geom
