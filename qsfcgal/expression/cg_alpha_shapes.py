# QGIS
from qgis.core import QgsGeometry, QgsWkbTypes
from qgis.utils import qgsfunction

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


@qgsfunction(args="auto", group="SFCGAL", register=False)
def cg_alpha_shapes(value, alpha: float, allow_holes: bool, feature, parent):
    """
    <style>
    span { color: red }

    </style>
    <h1>Description</h1>

    Computes the Alpha-Shape of the points in a geometry. An alpha-shape is a (usually)
    concave polygonal geometry which contains all the vertices of the input, and whose
    vertices are a subset of the input vertices. An alpha-shape provides a closer
    fit to the shape of the input than the shape produced by the convex hull.
    The 'closeness of fit' is controlled by the alpha parameter, which can have
    values from 0 to infinity. Smaller alpha values produce more concave results.
    Alpha values greater than some data-dependent value produce the convex hull of the input.
    The computed shape does not contain holes unless the optional allow_holes argument is specified as true.
    This function effectively computes a concave hull of a geometry in a similar way to ST_ConcaveHull,
    but uses CGAL and a different algorithm.

    <br/><br/>
    <h1>Auguments</h1>
    <table>
        <tr><td><span>input</span></td><td>geometry</td></tr>
        <tr><td><span>alpha</span></td><td>float</td></tr>
        <tr><td><span>allow holes</span></td><td>bool</td></tr>
    </table>
    <br/><br/>
    <h1>Syntax</h1>
    cg_alpha_shapes(<span>@geometry</span>,<span>alpha</span>,<span>allow holes</span>)
    <br/><br/>
    <h1>Examples</h1>
    <ul>
        <li> cg_alpha_shapes(geom_from_wkt('MultiPoint ((3.3 5.6), (3.4 5.3), (3.6 5.7), (3.6 5.3), (3.4 5.5), (3.8 5.5))'), 80, False) -> 'POLYGON((3.60 5.70,3.80 5.50,3.60 5.30,3.40 5.30,3.30 5.60,3.60 5.70))'</li>
    </ul>
    """

    if not value.wkbType() == QgsWkbTypes.MultiPoint:
        return None

    geom_input = QgsGeometry_to_SFCGAL(value)
    result_geom = geom_input.alpha_shapes(alpha, allow_holes)
    new_geom = SFCGAL_to_QgsGeometry(result_geom)
    return new_geom
