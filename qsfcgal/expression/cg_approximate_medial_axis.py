# QGIS
from qgis.core import QgsGeometry
from qgis.utils import qgsfunction

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


@qgsfunction(
    args="auto",
    group="SFCGAL",
    register=False,
    helpText=(
        "Return an approximate medial axis for the areal input based on its straight skeleton."
    ),
)
def cg_approximate_medial_axis(value, feature, parent):
    """
    <style>
    span { color: red }

    </style>
    <h1>Description</h1>

    Return an approximate medial axis for the areal input based on its straight skeleton.
    <br/><br/>
    <h1>Auguments</h1>
    <table>
        <tr><td><span>geometry</span></td><td>Polygon geometry</td></tr>
    </table>
    <br/><br/>
    <h1>Syntax</h1>
    cg_approximate_medial_axis(<span>@geometry</span>)
    <br/><br/>
    <h1>Examples</h1>
    <ul>
        <li>Polygon ((2.82738680597299563 0.88666941849432757, 2.80207035027679296 0.43097321596268223,
        1.02991845154261563 0.6208466336842009, 0.78941212242869163 -0.60700146758162177, 2.54890579331476719
        -0.63231792327782443, 2.58688047685907119 -1.29054577137909021, 0.38434883128945119 -1.16396349289807777,
        0.68814629964388141 0.88666941849432757, 2.82738680597299563 0.88666941849432757))
        -> MultiLineString ((0.92761544292438147 0.75960217383278561, 0.85169310058546732 0.69710845072452376),
        (0.92761544292438147 0.75960217383278561, 2.598700447829005 0.67033521886697156),
        (0.60125651339677322 -0.75962011862290835, 0.71573334163888025 -0.89425203249408136),
        (0.60125651339677322 -0.75962011862290835, 0.85169310058546732 0.69710845072452376),
        (0.71573334163888025 -0.89425203249408136, 2.24544854349499312 -0.94918905156919664))	0</li>
    </ul>
    """
    geom_input = QgsGeometry_to_SFCGAL(value)
    if not geom_input.geom_type in ["Polygon", "MultiPolygon"]:
        return None

    result_geom = geom_input.approximate_medial_axis()
    new_geom = SFCGAL_to_QgsGeometry(result_geom)
    return new_geom
