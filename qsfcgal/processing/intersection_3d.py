"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

# QGIS
from qgis import processing
from qgis.core import (
    Qgis,
    QgsFeature,
    QgsFeatureRequest,
    QgsFeatureSink,
    QgsGeometry,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSink,
    QgsProcessingParameterFeatureSource,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QByteArray, QCoreApplication

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


class Intersection_3D(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    OVERLAY = "OVERLAY"
    OUTPUT = "OUTPUT"
    FORCE_SOLID = "FORCE_SOLID"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Intersection_3D()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Intersection_3D"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Intersection 3D")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("3D")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "3d"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "The algorithm identifies the common spatial 3D elements shared by two or more layers. "
            "An intersection represents the portion where features from different layers overlap."
            "The algorithm analyzes the geometric relationships, such as polygons 3D "
            "within the input layers to precisely determine these intersecting areas. "
            "The output is a new layer or dataset highlighting the spatial overlap."
            ""
            ""
            "Warning: due to an error in QGIS's handling of 3D data, if you run processing with "
            "temporary output, you'll get an error or empty geometry output. You must specify an output to a GeoPackage."
            ""
            "MultiPolygonZ geometries are considered by QgsProcessing as invalid geometries. "
            "The 'Invalid feature filtering' parameter must therefore be set to 'Do not Filter'."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT,
                description=self.tr("Input layer"),
                types=[QgsProcessing.TypeVector],
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.OVERLAY,
                description=self.tr("Overlay layer"),
                types=[QgsProcessing.TypeVector],
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                name=self.FORCE_SOLID,
                description=self.tr(
                    "Convert PolyhedralSurface to Solid for intersection"
                ),
                defaultValue=True,
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Intersection 3D"),
                type=QgsProcessing.TypeVector,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        source = self.parameterAsSource(parameters, self.INPUT, context)
        overlay = self.parameterAsSource(parameters, self.OVERLAY, context)
        force_solid = self.parameterAsBoolean(parameters, self.FORCE_SOLID, context)

        (sink, dest_id) = self.parameterAsSink(
            parameters=parameters,
            name=self.OUTPUT,
            context=context,
            fields=source.fields(),
            geometryType=QgsWkbTypes.PolyhedralSurfaceZ,
            crs=source.sourceCrs(),
        )

        # 1015 = Polyhedral Surface
        if source.wkbType() != 1015 and overlay.wkbType() != 1015:
            raise QgsProcessingException(
                "To make a 3D intersection the two layers must be of type PolyhedralSurface Z"
            )

        if source is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT)
            )

        if overlay is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.OVERLAY)
            )

        feedback.pushInfo("CRS is {}".format(source.sourceCrs().authid()))

        # WARNING
        # Processing considers a Polyhedral to be an invalid geometry.
        # QgsFeatureRequest(), Qgis.ProcessingFeatureSourceFlag.SkipGeometryValidityChecks :
        # Is a work around that should be removed once the bug has been fixed in QGIS.
        # TODO: drop this workaround
        features_source = source.getFeatures(
            QgsFeatureRequest(),
            Qgis.ProcessingFeatureSourceFlag.SkipGeometryValidityChecks,
        )
        features_overlay = overlay.getFeatures(
            QgsFeatureRequest(),
            Qgis.ProcessingFeatureSourceFlag.SkipGeometryValidityChecks,
        )

        # Source
        for current, feature_source in enumerate(features_source):
            if feedback.isCanceled():
                break

            geom_source = feature_source.geometry()
            if geom_source:
                geom_source_sfcgal = QgsGeometry_to_SFCGAL(geom_source)
                if force_solid:
                    geom_source_sfcgal = geom_source_sfcgal.to_solid()

                # Overlay
                for current_overlay, feature_overlay in enumerate(features_overlay):
                    if feedback.isCanceled():
                        break

                    geom_overlay = feature_overlay.geometry()
                    if geom_overlay:
                        geom_overlay_sfcgal = QgsGeometry_to_SFCGAL(geom_overlay)
                        # Force solid
                        geom_overlay_sfcgal = geom_overlay_sfcgal.to_solid()

                    # 3D Intersection
                    test_intersection = geom_source_sfcgal.intersects_3d(
                        geom_overlay_sfcgal
                    )
                    if test_intersection:
                        result = geom_source_sfcgal.intersection_3d(geom_overlay_sfcgal)

                        # QGIS not supported SOLID, so we convert result to POLYHEDRAL
                        result = result.to_polyhedralsurface()
                        # Convert geom result to QgsGeometry
                        new_geom = SFCGAL_to_QgsGeometry(result)

                        feature_source.setGeometry(new_geom)

                        sink.addFeature(feature_source, QgsFeatureSink.FastInsert)

            else:
                feedback.pushInfo(
                    f"Entity {feature_source.id()} not added to the result layer because his input geometry is null."
                )

        return {self.OUTPUT: dest_id}
