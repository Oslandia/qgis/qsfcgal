"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

# QGIS
from qgis import processing
from qgis.core import (
    QgsFeature,
    QgsFeatureSink,
    QgsGeometry,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterFeatureSink,
    QgsProcessingParameterFeatureSource,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QByteArray, QCoreApplication

# Projet
from qsfcgal.core.sfcgal_wrapper import (
    QgsGeometry_to_SFCGAL,
    SFCGAL_to_QgsGeometry,
    sfcgal,
)

# Geom
POINT = QgsWkbTypes.Point
MULTIPOINT = QgsWkbTypes.MultiPoint
LINESTRING = QgsWkbTypes.LineString
MULTILINESTRING = QgsWkbTypes.MultiLineString
POLYGON = QgsWkbTypes.Polygon
MULTIPOLYGON = QgsWkbTypes.MultiPolygon


class Union(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    OVERLAY = "OVERLAY"
    OUTPUT = "OUTPUT"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Union()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Union"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Union")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("2D")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "geometry"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "The union algorithm returns all entities from the input layer. If a geometry "
            "from the input layer intersects with another from the overlay layer, these two "
            "geometries are returned merged. Geometries from the overlay layer that do not "
            "intersect with any geometry from the input layer are not returned."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT,
                description=self.tr("Input layer"),
                types=[QgsProcessing.TypeVector],
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.OVERLAY,
                description=self.tr("Overlay layer"),
                types=[QgsProcessing.TypeVector],
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Union"),
                type=QgsProcessing.TypeVector,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        source = self.parameterAsSource(parameters, self.INPUT, context)
        overlay = self.parameterAsSource(parameters, self.OVERLAY, context)

        if source is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT)
            )

        if overlay is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.OVERLAY)
            )

        g1 = source.wkbType()
        g2 = overlay.wkbType()

        if (
            ((g1 == POINT or g1 == MULTIPOINT) and g2 != POINT and g2 != MULTIPOINT)
            or (
                (g1 == POLYGON or g1 == MULTIPOLYGON)
                and g2 != POLYGON
                and g2 != MULTIPOLYGON
            )
            or (
                (g1 == LINESTRING or g1 == MULTILINESTRING)
                and g2 != LINESTRING
                and g2 != MULTILINESTRING
            )
        ):
            feedback.pushWarning(
                f"The two layers do not have the same geometry type; while SFCGAL handles this case, but QGIS is unable to represent a Geometry Collection in an output layer."
            )
            feedback.cancel()

        (sink, dest_id) = self.parameterAsSink(
            parameters=parameters,
            name=self.OUTPUT,
            context=context,
            fields=source.fields(),
            geometryType=source.wkbType(),
            crs=source.sourceCrs(),
        )

        feedback.pushInfo("CRS is {}".format(source.sourceCrs().authid()))
        total = 100.0 / source.featureCount() if source.featureCount() else 0

        features_source = source.getFeatures()
        features_overlay = list(overlay.getFeatures())

        for current, feature in enumerate(features_source):
            if feedback.isCanceled():
                break

            geom = feature.geometry()
            collection = sfcgal.GeometryCollection()
            geom_overlay = None

            if geom is not None:
                geom_input = QgsGeometry_to_SFCGAL(geom)

                # Checks which overlay geometries the current input geometry interesects
                for current_overlay, feature_overlay in enumerate(features_overlay):
                    geom_overlay = QgsGeometry_to_SFCGAL(feature_overlay.geometry())
                    if geom_input.intersects(geom_overlay):
                        collection.addGeometry(geom_overlay)

                if len(collection) > 0:
                    result_geom = geom_input.union(collection)
                    new_geom = SFCGAL_to_QgsGeometry(result_geom)

                # if input geometry no intersects any overlay geometry : return input geometry
                else:
                    new_geom = feature.geometry()

                feature.setGeometry(new_geom)
                sink.addFeature(feature, QgsFeatureSink.FastInsert)

                # Update the progress bar
                feedback.setProgress(int(current * total))
            else:
                sink.addFeature(feature, QgsFeatureSink.FastInsert)
                feedback.pushInfo(
                    f"Entity {feature.id()} added to the result layer but without a difference because his input geometry is null."
                )

        return {self.OUTPUT: dest_id}
