"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

# QGIS
from qgis import processing
from qgis.core import (
    QgsFeature,
    QgsFeatureSink,
    QgsGeometry,
    QgsMultiPoint,
    QgsPointXY,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSink,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterNumber,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QByteArray, QCoreApplication

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


class AlphaShapes(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    ALPHA = "ALPHA"
    ALLOW_HOLES = "ALLOW_HOLES"
    OUTPUT = "OUTPUT"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return AlphaShapes()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "AlphaShapes"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Alpha shapes")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("2D")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "geometry"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Computes the Alpha-Shape of the points in a geometry. An alpha-shape is a (usually) "
            "concave polygonal geometry which contains all the vertices of the input, and whose "
            "vertices are a subset of the input vertices. An alpha-shape provides a closer "
            "fit to the shape of the input than the shape produced by the convex hull. "
            "The 'closeness of fit' is controlled by the alpha parameter, which can have "
            "values from 0 to infinity. Smaller alpha values produce more concave results. "
            "Alpha values greater than some data-dependent value produce the convex hull of the input."
            ""
            "The computed shape does not contain holes unless the optional allow_holes argument is specified as true. "
            "This function effectively computes a concave hull of a geometry in a similar way to ST_ConcaveHull, "
            "but uses CGAL and a different algorithm."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT,
                description=self.tr("Input layer"),
                types=[QgsProcessing.TypeVectorPoint],
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.ALPHA,
                description=self.tr("Alpha"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=80,
                minValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                name=self.ALLOW_HOLES, description=self.tr("Allow holes")
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Alpha shapes"),
                type=QgsProcessing.TypeVectorPolygon,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        source = self.parameterAsSource(parameters, self.INPUT, context)
        alpha = self.parameterAsDouble(parameters, self.ALPHA, context)
        allow_holes = self.parameterAsBoolean(parameters, self.ALLOW_HOLES, context)

        if source is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT)
            )

        (sink, dest_id) = self.parameterAsSink(
            parameters=parameters,
            name=self.OUTPUT,
            context=context,
            fields=source.fields(),
            geometryType=QgsWkbTypes.Polygon,
            crs=source.sourceCrs(),
        )

        feedback.pushInfo("CRS is {}".format(source.sourceCrs().authid()))

        total = 100.0 / source.featureCount() if source.featureCount() else 0
        features = source.getFeatures()

        # In the case of a point-type layer, all points are grouped together in a MultiPoint. Then we apply the algorithm to this mulitpoint.
        if source.wkbType() == QgsWkbTypes.Point:
            multi_point = QgsGeometry()
            for current, feature in enumerate(features):
                if feedback.isCanceled():
                    break

                geom = feature.geometry()
                if geom is not None:
                    if geom.type() == QgsWkbTypes.PointGeometry:
                        if multi_point.isEmpty():
                            multi_point = geom
                        else:
                            multi_point = multi_point.combine(geom)
                feedback.setProgress(int(current * total))

            geom_sfcgal = QgsGeometry_to_SFCGAL(multi_point)
            result_geom = geom_sfcgal.alpha_shapes(alpha, allow_holes)
            new_geom = SFCGAL_to_QgsGeometry(result_geom)
            feature.setGeometry(new_geom)
            sink.addFeature(feature, QgsFeatureSink.FastInsert)

        # In the case of a MultiPoint layer, processing is iterated on each MultiPoint.
        if source.wkbType() == QgsWkbTypes.MultiPoint:
            for current, feature in enumerate(features):
                if feedback.isCanceled():
                    break

                geom = feature.geometry()

                if geom is not None:
                    try:
                        geom_sfcgal = QgsGeometry_to_SFCGAL(geom)
                        result_geom = geom_sfcgal.alpha_shapes(alpha, allow_holes)
                        new_geom = SFCGAL_to_QgsGeometry(result_geom)

                    except:
                        feedback.pushInfo(
                            f"Error with the entity {feature.id()}, it will be added to the result but without alpha shapes polygon."
                        )
                        new_geom = QgsGeometry()
                    feature.setGeometry(new_geom)
                    sink.addFeature(feature, QgsFeatureSink.FastInsert)

                    # Update the progress bar
                    feedback.setProgress(int(current * total))

        return {self.OUTPUT: dest_id}
