"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

# QGIS
from qgis import processing
from qgis.core import (
    QgsFeature,
    QgsFeatureSink,
    QgsGeometry,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSink,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterNumber,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QCoreApplication

# Projet
from qsfcgal.core.sfcgal_wrapper import QgsGeometry_to_SFCGAL, SFCGAL_to_QgsGeometry


class Translate_3D(QgsProcessingAlgorithm):
    INPUT = "INPUT"
    OUTPUT = "OUTPUT"
    TRANSLATE_X = "TRANSLATE_X"
    TRANSLATE_Y = "TRANSLATE_Y"
    TRANSLATE_Z = "TRANSLATE_Z"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Translate_3D()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Translate_3D"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Translate_3D")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("3D")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "3d"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "3D translation is an algorithm that shifts geometric objects (such as points, lines, or polygons) within 3D space (x, y, z). The translation alters the coordinates of the features by adding specific values to the x, y (horizontal) and z (vertical) axes. This allows repositioning of the objects by adjusting their location in a 3D context."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT,
                description=self.tr("Input layer"),
                types=[QgsProcessing.TypeVector],
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.TRANSLATE_X,
                description=self.tr("Translate_3D X"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.TRANSLATE_Y,
                description=self.tr("Translate_3D Y"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.TRANSLATE_Z,
                description=self.tr("Translate_3D Z"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0,
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Translate_3D"),
                type=QgsProcessing.TypeVectorPolygon,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        source = self.parameterAsSource(parameters, self.INPUT, context)

        translate_3d_x = self.parameterAsDouble(parameters, self.TRANSLATE_X, context)
        translate_3d_y = self.parameterAsDouble(parameters, self.TRANSLATE_Y, context)
        translate_3d_z = self.parameterAsDouble(parameters, self.TRANSLATE_Z, context)

        if source is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT)
            )

        feedback.pushInfo("CRS is {}".format(source.sourceCrs().authid()))

        features = source.getFeatures()
        list_result_features = []
        for current, feature in enumerate(features):
            if feedback.isCanceled():
                break

            geom = feature.geometry()

            if geom is not None:
                geom_sfcgal = QgsGeometry_to_SFCGAL(geom)
                geom_sfcgal_translate_3d = geom_sfcgal.translate_3d(
                    translate_3d_x, translate_3d_y, translate_3d_z
                )
                geom_qgis_translate_3d = SFCGAL_to_QgsGeometry(geom_sfcgal_translate_3d)
                output_wkbtype = geom_qgis_translate_3d.wkbType()
                feature.setGeometry(geom_qgis_translate_3d)
                list_result_features.append(feature)
            else:
                feedback.pushInfo(
                    f"Entity {feature.id()} not added to the result layer because its input geometry is null."
                )

        (sink, dest_id) = self.parameterAsSink(
            parameters=parameters,
            name=self.OUTPUT,
            context=context,
            fields=source.fields(),
            geometryType=output_wkbtype,
            crs=source.sourceCrs(),
        )
        for feature in list_result_features:
            sink.addFeature(feature, QgsFeatureSink.FastInsert)

        return {self.OUTPUT: dest_id}
