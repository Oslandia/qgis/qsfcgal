"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

# QGIS
from qgis import processing
from qgis.core import (
    QgsFeature,
    QgsFeatureSink,
    QgsGeometry,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSink,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterNumber,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QByteArray, QCoreApplication

# Projet
from qsfcgal.core.sfcgal_wrapper import (
    QgsGeometry_to_SFCGAL,
    SFCGAL_to_QgsGeometry,
    multipolygon_to_polygons,
)


class Extrude(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    OUTPUT = "OUTPUT"
    EXTRUDE_X = "EXTRUDE_X"
    EXTRUDE_Y = "EXTRUDE_Y"
    EXTRUDE_Z = "EXTRUDE_Z"
    FORCE_SINGLEPART = "FORCE_SINGLEPART"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Extrude()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Extrude"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Extrude")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("3D")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "3d"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Extruding a polygon involves extending or prolonging the sides of a polygon in a specified direction, usually perpendicular to the plane of the polygon. This operation creates a new three-dimensional shape using the initial polygon as a base, and using x, y and z values to determine the dimensions and direction of the extrusion in three-dimensional space."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT,
                description=self.tr("Input layer"),
                types=[QgsProcessing.TypeVectorPolygon],
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.EXTRUDE_X,
                description=self.tr("Extrude X"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.EXTRUDE_Y,
                description=self.tr("Extrude Y"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.EXTRUDE_Z,
                description=self.tr("Extrude Z"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                name=self.FORCE_SINGLEPART,
                description=self.tr(
                    "Forced explosion of MULTIPOLYGON into POLYGON if necessary for successful treatment."
                ),
                defaultValue=True,
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Extrude"),
                type=QgsProcessing.TypeVectorPolygon,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        source = self.parameterAsSource(parameters, self.INPUT, context)

        extrude_x = self.parameterAsDouble(parameters, self.EXTRUDE_X, context)
        extrude_y = self.parameterAsDouble(parameters, self.EXTRUDE_Y, context)
        extrude_z = self.parameterAsDouble(parameters, self.EXTRUDE_Z, context)

        force_singlepart = self.parameterAsBoolean(
            parameters, self.FORCE_SINGLEPART, context
        )

        if source is None:
            raise QgsProcessingException(
                self.invalidSourceError(parameters, self.INPUT)
            )

        (sink, dest_id) = self.parameterAsSink(
            parameters=parameters,
            name=self.OUTPUT,
            context=context,
            fields=source.fields(),
            geometryType=QgsWkbTypes.PolyhedralSurfaceZ,
            crs=source.sourceCrs(),
        )

        feedback.pushInfo("CRS is {}".format(source.sourceCrs().authid()))

        features = source.getFeatures()

        for current, feature in enumerate(features):
            if feedback.isCanceled():
                break

            geom = feature.geometry()

            if geom is not None:
                if geom.isMultipart() and force_singlepart:
                    feedback.pushWarning(
                        f"Please note that the {feature.id()} entity is of the MULTIPOLYGON type. Extrude only supports non-mulitpartite polygons, so this entity must be split into several parts in order to run extrude."
                    )
                    polygons = multipolygon_to_polygons(geom)
                    for elem in polygons:
                        geom_singlepart_sfcgal = QgsGeometry_to_SFCGAL(elem)
                        geom_sfcgal_singlepart_extrude = geom_singlepart_sfcgal.extrude(
                            extrude_x, extrude_y, extrude_z
                        )
                        result_sfcgal = (
                            geom_sfcgal_singlepart_extrude.to_polyhedralsurface()
                        )
                        geom_singlepart_qgis = SFCGAL_to_QgsGeometry(result_sfcgal)
                        feature.setGeometry(geom_singlepart_qgis)
                        sink.addFeature(feature, QgsFeatureSink.FastInsert)

                elif geom.isMultipart() and not force_singlepart:
                    raise QgsProcessingException(
                        "You've chosen not to force single-part geometries, but your geometries are mulitpolygon. Processing cannot be performed. Extrude does not (yet) support mulitpolygon."
                    )

                else:
                    geom_sfcgal = QgsGeometry_to_SFCGAL(geom)
                    geom_sfcgal_extrude = geom_sfcgal.extrude(
                        extrude_x, extrude_y, extrude_z
                    )
                    geom_sfcgal_extrude = geom_sfcgal_extrude.to_polyhedralsurface()
                    geom_qgis_extrude = SFCGAL_to_QgsGeometry(geom_sfcgal_extrude)
                    feature.setGeometry(geom_qgis_extrude)
                    sink.addFeature(feature, QgsFeatureSink.FastInsert)

            else:
                feedback.pushInfo(
                    f"Entity {feature.id()} not added to the result layer because its input geometry is null."
                )

        return {self.OUTPUT: dest_id}
