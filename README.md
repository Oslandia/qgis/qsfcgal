# QSGCFAL

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

## Description

Implementation of the SFCGAL library in a QGIS plugin using the python wrapper pysfcgal.

[QGIS](https://qgis.org) is the most dynamic OpenSource Geographical Information System platform, including the QGIS Desktop software application.

## Algorithm available in the plugin

✅ : Available  |   ⌛️ : Soon !

### 2D

| Algorithm  | Processing        | Expression     | Output |
|:--------------:|:---------------:|:--------------:|:----------------:|
| straight_skeleton               |✅   |   ✅   | Geometry         |
| convexhull                      |✅  |✅  | Geometry         |
| difference                      |✅  |✅  | Geometry         |
| intersection                    |✅  |✅  | Geometry         |
| union     |✅  |✅  | Geometry         |
| triangulate_2dz                 |⌛️  |⌛️  | Geometry         |
| tessellate                      |⌛️  |⌛️  | Geometry         |
| force_lhr |✅  |✅  | Geometry         |
| force_rhr |✅  |✅  | Geometry         |
| translate |✅  |✅  | Geometry         |
| minkowski_sum                   |✅  |✅  | Geometry         |
| offset_polygon                  |✅  |✅  | Geometry         |
| straight_skeleton_distance_in_m |⌛️  |⌛️  | Geometry         |
| approximate_medial_axis         |✅  |✅  | Geometry         |
| line_sub_string                 |✅  |✅  | Geometry         |
| alpha_shapes                    |✅  |✅  | Geometry         |
| optimal_alpha_shapes            |✅  |✅  | Geometry         |
| approximate_medial_axis         |✅  |✅  | Geometry         |
| line_sub_string                 |✅  |✅  | Geometry         |
| alpha_shapes                    |✅  |✅  | Geometry         |
| optimal_alpha_shapes            |✅  |✅  | Geometry         |
| y_monotone_partition_2          |⌛️  |⌛️  | Geometry         |
| approx_convex_partition_2       |⌛️  |⌛️  | Geometry         |
| greene_approx_convex_partition_2|⌛️  |⌛️  | Geometry         |
| optimal_convex_partition_2      |⌛️  |⌛️  | Geometry         |
| point_visibility                |⌛️  |⌛️  | Geometry         |
| segment_visibility              |⌛️  |⌛️  | Geometry         |
| intersects |⌛️ |⌛️ | Bool             |
| covers     |⌛️ |⌛️ | Bool             |
| orientation|⌛️ |⌛️ | int              |

### 3D

| Algorithm                      | Processing         | Expression     | Output |
|:--------------:|:---------------:|:--------------:|:----------------:|
| convexhull_3d                 |⌛️  |⌛️  | Geometry         |
| difference_3d                 |⌛️  |⌛️  | Geometry         |
| intersection_3d               |✅  |✅  | Geometry         |
| extrude                       |✅  |✅  | Geometry         |
| union_3d                      |⌛️  |⌛️  | Geometry         |
| translate_3D |✅  |✅  | Geometry         |
| extrude_straight_skeleton     |⌛️  |⌛️  | Geometry         |
| extrude_polygon_straight_skeleton|⌛️          |⌛️  | Geometry         |
| area_3d    |⌛️          |⌛️ | Float            |
| intersects_3d |⌛️       |⌛️ | Bool             |
| covers_3d  |⌛️          |⌛️ | Bool             |

## Requirements

:warning: This plugin does **not include** the `pysfcgal` dependencies required for its proper operation. You must therefore make sure that you have the `pysfcgal` module installed on the python environment used by QGIS. For more information, see the pysfcgal [page](https://gitlab.com/sfcgal/pysfcgal).

## Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

🇬🇧 [Check-out the documentation](https://oslandia.gitlab.io/qgis/qsfcgal/)

## Credits

This plugin has been developed by Oslandia ( <https://oslandia.com> ).

Oslandia provides support and assistance for QGIS and associated tools, including this plugin.

## License

Distributed under the terms of the [`GPL` license version 2 or later](LICENSE).

## Contributing

Contributions are welcome ! You can contribute through :

- Testing and [giving feedback in issues](https://gitlab.com/Oslandia/qgis/qduckdb/-/issues)
- Bug reports & bug fixes
- Code for new features ( please open an issue for discussion before coding )
- Documentation
