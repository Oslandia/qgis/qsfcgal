stages:
  - 🐍 lint
  - 🦗 container-qgis-sfcgal
  - 🤞 test
  - 📦 build
  - 🚀 deploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_FOLDER/.cache/pip"
  PROJECT_FOLDER: "qsfcgal"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PROJECT_FOLDER}"
  PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  PYTHON_MINIMAL_VERSION: "3.12"
  PYTHON_TRANSLATION_VERSION: "3.11" # Waiting for Python 3.12 being available for pyqt5-tools. See: https://github.com/altendky/pyqt-tools/issues/131
  REPO_PLUGIN_URL: "https://gitlab.com/Oslandia/qgis/qsfcgal"
  QGIS_VERSION: "3.34"
  SFCGAL_VERSION: "2.0.0"
  CGAL_VERSION: "5.6"
  DEBIAN_IMAGE: "slim-bookworm"
  CONTAINER_TEST_IMAGE: ${CI_REGISTRY_IMAGE}/qgis_sfcgal:${QGIS_VERSION}
  PYTHON_MAIN_IMAGE: python:${PYTHON_MINIMAL_VERSION}-${DEBIAN_IMAGE}

cache:
  key:
    files:
      - requirements/*.txt
  paths:
    - ${PIP_CACHE_DIR}
    - ${PRE_COMMIT_HOME}

# -- LINT JOBS -------------------------------------------------------------------------
git-hooks:
  stage: 🐍 lint
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}
  only:
    refs:
      - merge_requests
  before_script:
    - apt update
    - apt install -y --no-install-recommends git
    - python3 -m pip install -U pip
    - python3 -m pip install -U setuptools wheel
    - python3 -m pip install -U pre-commit
    - pre-commit install
    - git fetch origin
  script:
    - pre-commit run --from-ref "origin/$CI_DEFAULT_BRANCH" --to-ref "$CI_COMMIT_SHA";


flake8:
  stage: 🐍 lint
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  only:
    changes:
      - "**/*.py"
  before_script:
    - python -m pip install -U flake8
  script:
    - flake8 $PROJECT_FOLDER --count --select=E9,F63,F7,F82 --show-source --statistics
    - flake8 $PROJECT_FOLDER --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics

# -- CONTAINERS JOBS -------------------------------------------------------------------
container-qgis-sfcgal:
  stage: 🦗 container-qgis-sfcgal
  image: docker:latest
  only:
      refs:
        - merge_requests
      changes:
        - "containers/**/*.dockerfile"
        - "containers/**/Dockerfile"
        - "requirements/testing.txt"
        - ".gitlab-ci.yml"
        - "**/*.py"
  services:
    - docker:dind
  tags:
    - oslandia_linux_vm
  variables:
    SFCGAL_REGISTRY: registry.gitlab.com/sfcgal/sfcgal
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - echo "Building image"
    - docker build
      --cache-from $CONTAINER_TEST_IMAGE
      --tag $CONTAINER_TEST_IMAGE
      --pull
      --build-arg="SFCGAL_REGISTRY=${SFCGAL_REGISTRY}"
      --build-arg="QGIS_VERSION=${QGIS_VERSION}"
      -f "containers/Dockerfile"
      .
    - echo "Push image"
    - docker push $CONTAINER_TEST_IMAGE
  after_script:
    - docker logout ${CI_REGISTRY}

# -- TEST JOBS --------------------------------------------------------------------------
tests:unit:
  stage: 🤞 test
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  only:
    changes:
      - "**/*.py"
      - ".gitlab-ci.yml"
  before_script:
    - python3 -m pip install -U -r requirements/development.txt
    - python3 -m pip install -U -r requirements/testing.txt
  script:
    - env PYTHONPATH=/usr/share/qgis/python:. pytest -p no:qgis tests/unit --junitxml=junit/test-results-unit.xml --cov-report=xml:coverage-reports/coverage-unit.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    paths:
      - coverage-reports/coverage-unit.xml
      - junit/test-results-unit.xml
    reports:
      junit: junit/test-results-unit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage-reports/coverage-unit.xml
    when: always

tests:qgis:
  stage: 🤞 test
  image: ${CONTAINER_TEST_IMAGE}
  tags :
    - oslandia_docker
  rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      - if: $CI_COMMIT_TAG
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    DISPLAY: ":1"
    PYTHONPATH: "/usr/share/qgis/python/plugins:/usr/share/qgis/python:."
  script:
    - xvfb-run -a pytest tests/qgis --junitxml=junit/test-results-qgis.xml --cov-report=xml:coverage-reports/coverage-qgis.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    paths:
      - coverage-reports/coverage-qgis.xml
      - junit/test-results-qgis.xml
    reports:
      junit: junit/test-results-qgis.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage-reports/coverage-qgis.xml
    when: always

# -- BUILD JOBS -------------------------------------------------------------------------
build:translation:
  stage: 📦 build
  image: python:${PYTHON_TRANSLATION_VERSION}-${DEBIAN_IMAGE}
  tags :
    - oslandia_docker
  rules:
    # on default branch
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
    # on a tag
    - if: $CI_COMMIT_TAG
    # on a MR if previous jobs are successful
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
  before_script:
    - apt update
    - apt install -y --no-install-recommends qt5-qmake qttools5-dev-tools
    - python -m pip install -U pyqt5-tools # Installing pyqt5-tools from apt may cause some issues
  script:
    - pylupdate5 -noobsolete -verbose $PROJECT_FOLDER/resources/i18n/plugin_translation.pro
    - lrelease $PROJECT_FOLDER/resources/i18n/*.ts
  artifacts:
    name: ui-translation
    paths:
      - $PROJECT_FOLDER/resources/i18n/*qm
    when: always

build:windows:
  stage: 📦 build
  tags:
    - oslandia_windows
  rules:
    # on default branch
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
    # on a tag
    - if: $CI_COMMIT_TAG
    # on a MR if previous jobs are successful
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
  before_script:
    - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
    - choco install -y python --version=3.12.3
    - RefreshEnv
  script:
    - python --version
    - python -m pip install -U pip setuptools wheel
    - python -m pip install --no-deps -U -r requirements/embedded.txt -t $PROJECT_FOLDER/embedded_external_libs

  artifacts:
    paths:
      - $PROJECT_FOLDER/embedded_external_libs
    untracked: true

build:plugin:
  stage: 📦 build
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  rules:
    # on default branch
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
    # on a tag
    - if: $CI_COMMIT_TAG
    # on a MR if previous jobs are successful
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: on_success
  needs:
    - build:translation
    - build:windows
  before_script:
    - apt update
    - apt install -y --no-install-recommends git
    - python -m pip install -U -r requirements/packaging.txt
  script:
    # Amend gitignore to include embedded libs with qgis-plugin-ci
    - sed -i "s|^$PROJECT_FOLDER/embedded_external_libs/.*| |" .gitignore
    # IMPORTANT: following lines are commented out since they are already in .gitignore
    # - echo -e "!$PROJECT_FOLDER/embedded_external_libs/**/*.so" >> .gitignore
    # - echo -e "!$PROJECT_FOLDER/embedded_external_libs/**/*.pyd" >> .gitignore

    # Amend gitignore to include translation with qgis-plugin-ci
    - sed -i "s|^*.qm.*| |" .gitignore

    # git tracks new files
    - git add $PROJECT_FOLDER/

    # Package the latest version listed in the changelog
    - qgis-plugin-ci package latest --allow-uncommitted-changes --plugin-repo-url $REPO_PLUGIN_URL
    - qgis-plugin-ci changelog latest >> RELEASE_DESCRIPTION.md
  artifacts:
    name: "$PROJECT_FOLDER_b$CI_COMMIT_REF_NAME-c$CI_COMMIT_SHORT_SHA-j$CI_JOB_ID"
    paths:
      - "${PROJECT_FOLDER}.*.zip"
      - plugins.xml
      - RELEASE_DESCRIPTION.md

build:documentation:
  stage: 📦 build
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  only:
    refs:
      - main
  before_script:
    - python -m pip install -U -r requirements/documentation.txt
  script:
    - sphinx-build -b html docs target/docs
  artifacts:
    name: documentation
    expose_as: "Built documentation static website"
    paths:
      - target/docs
    when: always

# -- DEPLOYMENT JOBS -------------------------------------------------------------------
pages:
  stage: 🚀 deploy
  tags :
    - oslandia_docker
  variables:
    GIT_STRATEGY: none
  only:
    changes:
      - "**/*.md"
      - "**/*.rst"
      - ".gitlab-ci.yml"
      - "$PROJECT_FOLDER/**/*"
      - requirements/documentation.txt
      - requirements/packaging.txt
    refs:
      - main
  needs:
    - job: build:plugin
      artifacts: true
    - job: build:documentation
      artifacts: true

  script:
    - mkdir -p public
    # copy generated plugin
    - cp ${PROJECT_FOLDER}.*.zip public/
    - cp plugins.xml public/
    # copy HTML documentation
    - cp -rf target/docs/* public/

  artifacts:
    paths:
      - public
    when: always

release:upload:
  stage: 🚀 deploy
  image: curlimages/curl:latest
  tags :
    - oslandia_docker
  only:
    - tags
  needs:
    - job: build:plugin
      artifacts: true
  script:
    - |
      PACKAGE_NAME=$(ls ${PROJECT_FOLDER}.*.zip)
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${PACKAGE_NAME} ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${PACKAGE_NAME}
      echo "PACKAGE_NAME=$PACKAGE_NAME" >> release.env
  artifacts:
    reports:
      dotenv: release.env

release:prepare:
  stage: 🚀 deploy
  tags :
    - oslandia_docker
  image:
    name: alpine/git:latest
    entrypoint: [""]
  only:
    - tags
  allow_failure: true
  needs:
    - job: build:plugin
      artifacts: true
  script:
    - echo -e '\n\n## Technical changelog\n' >> RELEASE_DESCRIPTION.md
    - git tag -l -n9 $CI_COMMIT_TAG >> RELEASE_DESCRIPTION.md
    - echo -e '\n### Merges\n' >> RELEASE_DESCRIPTION.md
    - git log --merges --pretty="- %s (%h)" $(git tag --sort=-creatordate | head -2)...$(git tag --sort=-creatordate | head -1) >> RELEASE_DESCRIPTION.md
    - echo -e '\n### AUTHORS\n' >> RELEASE_DESCRIPTION.md
    - git log --pretty="- %an%n- %cn" $(git tag --sort=-creatordate | head -2)...$(git tag --sort=-creatordate | head -1) | sort | uniq >> RELEASE_DESCRIPTION.md
  artifacts:
    paths:
      - RELEASE_DESCRIPTION.md

release:publish:
  stage: 🚀 deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags :
    - oslandia_docker
  variables:
    GIT_STRATEGY: none
  only:
    - tags
  needs:
    - job: release:prepare
      artifacts: true
    - job: release:upload
      artifacts: true
  script:
    - echo "Creating release from $CI_COMMIT_TAG"
  release: # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    description: RELEASE_DESCRIPTION.md
    name: $CI_COMMIT_TAG
    tag_name: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: '${PACKAGE_NAME}'
          url: '${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${PACKAGE_NAME}'

deploy:qgis-repository:
  stage: 🚀 deploy
  image: ${PYTHON_MAIN_IMAGE}
  tags :
    - oslandia_docker
  only:
    - tags
  before_script:
    - apt update
    - apt install -y --no-install-recommends git
    - python -m pip install -U -r requirements/packaging.txt
  script:
    - echo "Deploying the version ${CI_COMMIT_TAG} plugin to QGIS Plugins Repository with the user ${OSGEO_USER_NAME}"
    - python ${PROJECT_FOLDER}/__about__.py
    - qgis-plugin-ci release ${CI_COMMIT_TAG}
        --osgeo-username $OSGEO_USER_NAME
        --osgeo-password $OSGEO_USER_PASSWORD
