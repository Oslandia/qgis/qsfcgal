# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.1.1 - 2025-02-07

- Update description in metadata.

## 0.1.0 - 2025-01-28

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
- 21 SFCGAL processes are implemented as processing and expression.
- Packaged version includes PySFCGAL dependency for Windows
