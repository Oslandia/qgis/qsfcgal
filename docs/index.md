# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Usage
maxdepth: 1
---
Installation <usage/installation>
Features <usage/features>
Sponsors <usage/sponsors>
```

```{toctree}
---
caption: QSFCGAL
maxdepth: 1
---
Algorithm <QSFCGAL/algorithm>
QSFCGAL/straight_skeleton
QSFCGAL/convexhull
QSFCGAL/difference
QSFCGAL/intersection
QSFCGAL/union
QSFCGAL/minkowski_sum
QSFCGAL/alpha_shapes
QSFCGAL/optimal_alpha_shapes
QSFCGAL/approximate_medial_axis
QSFCGAL/force_lhr
QSFCGAL/force_rhr
QSFCGAL/translate
QSFCGAL/translate_3d
QSFCGAL/intersection_3d
QSFCGAL/extrude
QSFCGAL/line_sub_string
QSFCGAL/offset_polygon
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```
