# Algorithm

✅ : Available  |   🔜 : Soon !

## 2D

| Algorithm  | Processing        | Expression     | Output |
|:--------------:|:---------------:|:--------------:|:----------------:|
| [straight_skeleton](./straight_skeleton.md)              |✅   |   ✅   | Geometry         |
| [convexhull](./convexhull.md)                      |✅  |✅  | Geometry         |
| [difference](./difference.md)                      |✅  |✅  | Geometry         |
| [intersection](./intersection.md)                    |✅  |✅  | Geometry         |
| [union](./union.md)     |✅  |✅  | Geometry         |
| [translate](./translate.md)     |✅  |✅  | Geometry         |
| triangulate_2dz                 |🔜  |🔜  | Geometry         |
| tessellate                      |🔜  |🔜  | Geometry         |
| [force_lhr](./force_lhr.md) |✅  |✅  | Geometry         |
| [force_rhr](./force_rhr.md) |✅  |✅  | Geometry         |
| [minkowski_sum](./minkowski_sum.md)                   |✅  |✅  | Geometry         |
| [offset_polygon](./offset_polygon.md)                  |✅  |✅  | Geometry         |
| straight_skeleton_distance_in_m |🔜  |🔜  | Geometry         |
| [approximate_medial_axis](./approximate_medial_axis.md)         |✅  |✅  | Geometry         |
| [line_sub_string](./line_sub_string.md)                 |✅  |✅  | Geometry         |
| [alpha_shapes](./alpha_shapes.md)                    |✅  |✅  | Geometry         |
| [optimal_alpha_shapes](./optimal_alpha_shapes.md)            |✅  |✅  | Geometry         |
| y_monotone_partition_2          |🔜  |🔜  | Geometry         |
| approx_convex_partition_2       |🔜  |🔜  | Geometry         |
| greene_approx_convex_partition_2|🔜  |🔜  | Geometry         |
| optimal_convex_partition_2      |🔜  |🔜  | Geometry         |
| point_visibility                |🔜  |🔜  | Geometry         |
| segment_visibility              |🔜  |🔜  | Geometry         |
| intersects |🔜 |🔜 | Bool             |
| covers     |🔜 |🔜 | Bool             |
| orientation|🔜 |🔜 | int              |

## 3D

| Algorithm                      | Processing         | Expression     | Type de Résultat |
|:--------------:|:---------------:|:--------------:|:----------------:|
| convexhull_3d                 |🔜  |🔜  | Geometry         |
| difference_3d                 |🔜  |🔜  | Geometry         |
| [intersection_3d](./intersection_3d.md)               |✅  |✅  | Geometry         |
| extrude                       |✅  |✅  | Geometry         |
| union_3d                      |🔜  |🔜  | Geometry         |
| [translate_3D](./translate_3d.md)     |✅  |✅  | Geometry         |
| extrude_straight_skeleton     |🔜  |🔜  | Geometry         |
| extrude_polygon_straight_skeleton|🔜          |🔜  | Geometry         |
| area_3d    |🔜          |🔜 | Float            |
| intersects_3d |🔜       |🔜 | Bool             |
| covers_3d  |🔜          |🔜 | Bool             |
