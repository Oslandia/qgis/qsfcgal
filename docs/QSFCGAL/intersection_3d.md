# 3D Intersection

## Definition

The algorithm identifies the common spatial elements shared by two layers. An intersection 3D represents the portion where features from different layers overlap. The algorithm analyzes the geometric relationships, such as points, lines, or polygons, within the input layers to precisely determine these intersecting areas. The output is a new layer or dataset highlighting the spatial overlap.

**Note:** This algorithm works only from QGIS **3.40**, as it requires support for the `PolyhedralSurfaceZ` geometry type.

## Example

### Input

```wkt
PolyhedralSurfaceZ (((0 0 0, 0 5 0, 5 5 0, 5 0 0, 0 0 0)),((0 0 7, 5 0 7, 5 5 7, 0 5 7, 0 0 7)),((0 0 0, 0 0 7, 0 5 7, 0 5 0, 0 0 0)),((0 5 0, 0 5 7, 5 5 7, 5 5 0, 0 5 0)),((5 5 0, 5 5 7, 5 0 7, 5 0 0, 5 5 0)),((5 0 0, 5 0 7, 0 0 7, 0 0 0, 5 0 0)))
```

### Overlay

```wkt
PolyhedralSurfaceZ (((3 3 0, 3 8 0, 8 8 0, 8 3 0, 3 3 0)),((3 3 7, 8 3 7, 8 8 7, 3 8 7, 3 3 7)),((3 3 0, 3 3 7, 3 8 7, 3 8 0, 3 3 0)),((3 8 0, 3 8 7, 8 8 7, 8 8 0, 3 8 0)),((8 8 0, 8 8 7, 8 3 7, 8 3 0, 8 8 0)),((8 3 0, 8 3 7, 3 3 7, 3 3 0, 8 3 0)))
```

### Force solid

This parameter is a Boolean. It indicates whether or not to transform the Polyhedral into a solid before proceeding with the intersection. It's advisable to set it to `True`, otherwise you'll get a geometry collection of `TIN Z` and `LINESTRING Z`.

```bool
True
```

![input](../img/3d_intersection_input_overlay.png)

### Output

```wkt
PolyhedralSurfaceZ (((5 3 7, 3 5 7, 3 3 7, 5 3 7)),((3 5 4.19999999999999929, 5 5 7, 3 5 2.79999999999999982, 3 5 4.19999999999999929)),((5 3 2.79999999999999982, 5 5 7, 5 3 4.19999999999999929, 5 3 2.79999999999999982)),((5 3 0, 3 3 0, 5 5 0, 5 3 0)),((3 3 0, 3 5 0, 5 5 0, 3 3 0)),((5 5 0, 5 3 2.79999999999999982, 5 3 0, 5 5 0)),((5 5 7, 3 5 4.19999999999999929, 3 5 7, 5 5 7)),((5 5 0, 3 5 2.79999999999999982, 5 5 7, 5 5 0)),((3 5 2.79999999999999982, 5 5 0, 3 5 0, 3 5 2.79999999999999982)),((5 5 7, 5 3 2.79999999999999982, 5 5 0, 5 5 7)),((5 3 4.19999999999999929, 5 5 7, 5 3 7, 5 3 4.19999999999999929)),((3 5 7, 5 3 7, 5 5 7, 3 5 7)),((3 5 2.79999999999999982, 3 3 0, 3 5 4.19999999999999929, 3 5 2.79999999999999982)),((5 3 4.19999999999999929, 5 3 7, 3 3 7, 5 3 4.19999999999999929)),((3 3 0, 3 5 2.79999999999999982, 3 5 0, 3 3 0)),((3 3 7, 3 5 4.19999999999999929, 3 3 0, 3 3 7)),((3 5 4.19999999999999929, 3 3 7, 3 5 7, 3 5 4.19999999999999929)),((5 3 2.79999999999999982, 3 3 7, 3 3 0, 5 3 2.79999999999999982)),((5 3 2.79999999999999982, 5 3 4.19999999999999929, 3 3 7, 5 3 2.79999999999999982)),((5 3 0, 5 3 2.79999999999999982, 3 3 0, 5 3 0)))
```

![output](../img/3d_result.png)
*Screenshot from QGIS 3.40*
