# Line sub string

## Définition

Computes the line which is the section of the input line starting and ending at the given fractional locations. The first argument must be a LINESTRING. The second and third arguments are values in the range [0, 1] representing the start and end locations as fractions of line length. The Z and M values are interpolated for added endpoints if present.

This process takes **Linestring** or **MultiLinestring** as input and outputs type as input.

## Exemple

### Input vector
```
MultiLineString ((-3.45239747876657699 2.98761427267796176, -3.01653687074822852 4.88955147130348244, -1.74857873833121413 4.96879885457954629, -0.44099691427616783 4.15651317599989678))
```

### Input start value
```
0.1
```

### Input end value
```
0.9
```

### Ouput
```
LineString (-3.34604772954282392 3.45168590565433941, -3.01653687074822852 4.88955147130348244, -1.74857873833121413 4.96879885457954629, -0.8454172674254945 4.40774400144114509)'
```

![](../img/line_sub_string.png)
