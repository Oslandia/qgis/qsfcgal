# Straight skeleton

## Définition

*from [wikipedia page](https://en.wikipedia.org/wiki/Straight_skeleton)*

> In geometry, a straight skeleton is a method of representing a polygon by a topological skeleton. It is similar in some ways to the medial axis but differs in that the skeleton is composed of straight line segments, while the medial axis of a polygon may involve parabolic curves.

This process takes **Polygon** or **MultiPolygon** as input and outputs **MultiLinestring**

## Example

### Input

```
MultiPolygon (((0.25953488 2.96837209, 2.87906977 1.7255814, -1.01302326 0.60930233, 3.07255814 3.42232558, 0.25953488 2.96837209)))
```

### Output
```
'MultiLineString ((2.25023256 0.68372093, 2.17259344 0.56509384),(1.06325581 0.66883721, 1.1700367 0.55596116),(0.95906977 -0.86046512, 1.1319602 -0.70689798),(2.3655814 -0.93116279, 2.15369017 -0.75887088),(2.28 -0.60372093, 2.15369017 -0.75887088),(1.26790698 -0.55162791, 1.11837114 -0.69158046),(1.2827907 0.45674419, 1.1781502 0.56405667),(2.35069767 0.4455814, 2.17259344 0.56509384),(1.1781502 0.56405667, 1.1700367 0.55596116),(1.1781502 0.56405667, 2.17259344 0.56509384),(1.1700367 0.55596116, 1.11837114 -0.69158046),(1.11837114 -0.69158046, 1.1319602 -0.70689798),(2.15369017 -0.75887088, 1.1319602 -0.70689798))'
```

![](../img/straight_skeleton.png)
