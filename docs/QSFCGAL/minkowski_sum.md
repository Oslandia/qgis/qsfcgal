# Minkowski sum

## Definition

This function performs a 2D minkowski sum of a point, line or polygon with a polygon. A minkowski sum of two geometries A and B is the set of all points that are the sum of any point in A and B.

This processing takes any type of geometry as input A, but input B must be of type Polygon (multipolygon is not accepted). The output will be a multipolygon. For example, you could use it for buffering.

## Example

### Input A

```
MultiLineString ((5.23274447450405589 2.24576273612754562, 7.86716307915521895 1.27832087566242869))
```

### Input B
```
Polygon ((8.01633423893374797 3.75917760095350939, 7.80743462162435797 1.57522705635533455, 5.60449320272532958 1.23339131893996701, 5.45256620831849936 3.60725060654668006, 8.01633423893374797 3.75917760095350939))
```

### Output

```
MultiPolygon (((13.24907871343780386 6.00494033708105501, 10.68531068282255525 5.85301334267422568, 10.83723767722938547 3.47915405506751263, 13.47165628188054853 2.5117121946023957, 15.67459770077957693 2.85354793201776324, 15.88349731808896692 5.03749847661593808, 13.24907871343780386 6.00494033708105501)))
```

![](../img/minkowski_sum.png)
