# Translate

## Definition

The algorithm performs a 3D translation of spatial elements within a layer by shifting their positions along the X and Y axes. Each feature in the input layer is moved according to the specified translation values for the X and Y axes. The output is a new layer where all features have been displaced based on the input parameters.

- **X**: The translation distance along the X-axis.
- **Y**: The translation distance along the Y-axis.

## Example

### Input

```wkt
Point (7.0 3.0)
```

### Input Parameters

- **Translate X**: 2.0
- **Translate  Y**: 1.0

### Output

```wkt
Point (9.0 4.0)
```

## Screenshot

![img](../img/translate.png)
