# Convex Hull

## Definition

A convex hull is the smallest convex shape that encloses a given set of points in space. It's like the tightest rubber band stretched around a set of points, forming the smallest convex polygon or polyhedron that contains all the points.

This process takes **MultiPoint** or **Point** as input and the type of output depends on the number of points.

- A single `Point` returns the same point as input
- Two points return a `LineString` between them
- After 2 points it returns a `Polygon`

## Example

### Input

```
MultiPoint ((5.01309128 6.00706177),(6.45681221 5.36706177),(3.76285872 5.64985247),(4.43262616 6.7363641),(5.12353046 5.63438799),(6.39727733 6.82566643))
```

### Output

```
Polygon ((3.76285872 5.64985247, 6.45681221 5.36706177, 6.39727733 6.82566643, 4.43262616 6.7363641, 3.76285872 5.64985247))
```

![](../img/convexhull.png)
