# Force LHR

## Définition

Forces the orientation of the vertices in a polygon to follow a Left-Hand-Rule, in which the area that is bounded by the polygon is to the left of the boundary. In particular, the exterior ring is orientated in a clockwise direction and the interior rings in a counter-clockwise direction.

This process takes **Polygon** or **MultiPolygon** as input and outputs **MultiPolygon**

## Example

### Input
```
MultiPolygon (((8.016 3.759, 7.807 1.575, 5.604 1.233, 5.453 3.607, 8.016 3.759)))
```

### Output
```
MultiPolygon (((8.016 3.759, 5.453 3.607, 5.604 1.233, 7.807 1.575, 8.016 3.759)))
```
No image is required, and the output geometics are similar. Only the vertex order has changed.
