# Translate_3D

## Definition

Same as [Translate](./translate.md) but with Z axis.

The algorithm performs a 3D translation of spatial elements within a layer by shifting their positions along the X, Y, and Z axes. Each feature in the input layer is moved according to the specified translation values for the X, Y, and Z axes. The output is a new layer where all features have been displaced in 3D space based on the input parameters.

- **X**: The translation distance along the X-axis.
- **Y**: The translation distance along the Y-axis.
- **Z**: The translation distance along the Z-axis.

## Example

### Input

```wkt
PointZ (7.0 3.0 2.0)
```

### Input Parameters

- **Translate X**: 2.0
- **Translate  Y**: 1.0
- **Translate  Z**: -1.0

### Output

```wkt
PointZ (9.0 4.0 1.0)
```
