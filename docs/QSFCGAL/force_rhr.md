# Force RHR

## Définition

Forces the orientation of the vertices in a polygon to follow a Right-Hand-Rule, in which the area that is bounded by the polygon is to the right of the boundary. In particular, the exterior ring is orientated in a clockwise direction and the interior rings in a counter-clockwise direction.

This process takes **Polygon** or **MultiPolygon** as input and outputs **MultiPolygon**

## Example

### Input
```
MULTIPOLYGON(((8.239 10.909,7.222 10.649,7.391 10.197,8.329 9.993,8.894 10.219,8.883 10.784,8.239 10.909)))
```

### Output
```
MULTIPOLYGON(((8.239 10.909,8.883 10.784,8.894 10.219,8.329 9.993,7.391 10.197,7.222 10.649,8.239 10.909)))
```

No image is required, and the output geometics are similar. Only the vertex order has changed.
