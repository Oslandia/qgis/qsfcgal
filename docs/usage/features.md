# Features

The plugin adds two different things to your QGIS environment:

## Expressions

New expressions can be found in a QSFCGAL section in the expression list. (see image)

![](../img/expression.png)

## Algorithm

The plugin also adds new processing to the toolbox. These can be used in the same way as classic processing. They can even be used in the graphic modeler for a processing chain.

![](../img/processing.png)

_______

See the [SFCGAL section](../QSFCGAL/algorithm.md) of the documentation for details of each algorithm.
