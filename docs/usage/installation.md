# Installation

## Stable version (recommended)

This plugin is published on the official QGIS plugins repository: <https://plugins.qgis.org/plugins/qsfcgal/>.

## Beta versions released

Enable experimental extensions in the QGIS plugins manager settings panel.

## Earlier development version

If you define yourself as early adopter or a tester and can't wait for the release, the plugin is automatically packaged for each commit to main, so you can use this address as repository URL in your QGIS extensions manager settings:

```
https://gitlab.com/Oslandia/qgis/qsfcgal/plugins.xml
```

Be careful, this version can be unstable.

## Requirements

The plugin's logic is mainly based on on the [PySFCGAL](https://sfcgal.gitlab.io/pysfcgal/), which is not distributed with QGIS.

### Windows: embedded package

It's quite a challenge to install it on QGIS for Windows, because QGIS uses its own Python interpreter and doesn't make it easy to use packages manager (`pip`).

To make it easier for Windows end-users, we did our best to embed the dependencies within the released version of the plugin (during CI), inside the `embedded_external_libs` folder.

Technically, the plugin tries to:

1. import packages from the Python interpreter used by QGIS (system on Linux, specific on Windows)
1. if it does not succeed, then it adds the `embedded_external_libs` subfolder to the `PYTHONPATH` and then import packages from it
1. it it still fails, then the plugin is disabled and the user is warned with a button leading him to here:

**BUT** there are some caveats because those packages require to be compiled with the same exact Python version than the one used by QGIS and sometimes the QGIS packagers change the Python version...
