We gratefully acknowledge the support of our sponsors. Their contributions help make this project possible.
QSFCGAL are sponsored by the following organizations:

| Sponsor        | Logo                                                                                          | Web/Description                                                                                  |
|----------------|-----------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| Oslandia       | ![Oslandia](../img/sponsors/oslandia.png)                            | [Oslandia](https://oslandia.com)                                                                      |
| BPI France     | ![BPI France](../img/sponsors/bpi_france.png)                      | [BPI France](https://www.bpifrance.com/)                                                              |
| France 2030    | ![France 2030](../img/sponsors/france_2030.png)                      | Funded by the French government as part of France 2030                                     |
| France Relance | ![France Relance](../img/sponsors/france_relance.png) ![EU](../img/sponsors/ue_next_generation.png) | Funded by the European Union - Next Generation EU as part of the France Relance plan |

## Funding

The first SFCGAL releases were funded by the European Union (FEDER, related to the [e-PLU project](http://www.e-plu.fr)) and by Oslandia.

We are seeking additional funding to continue development. If interested, contact us at [infos@oslandia.com](mailto:infos@oslandia.com).
