from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.optimal_alpha_shapes import OptimalAlphaShapes
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_optimal_alpha_shapes():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    multipoint = f"{bdd}|layername=random_points_multipoints"
    point = f"{bdd}|layername=random_points"

    # Point
    params = {
        OptimalAlphaShapes.INPUT: str(point),
        OptimalAlphaShapes.ALLOW_HOLES: False,
        OptimalAlphaShapes.NB_COMPONENTS: 1,
        OptimalAlphaShapes.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:OptimalAlphaShapes")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[OptimalAlphaShapes.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 1

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "Polygon ((21.554039 5.768618, 21.560184 5.477298, 21.543965 5.304157, 21.187587 5.223907, 21.056125 5.120547, 21.012423 5.130021, 20.705525 5.147055, 20.371948 5.125454, 20.147148 5.004265, 20.006931 4.935986, 19.910644 4.968801, 19.633629 4.987168, 19.382031 4.86593, 19.192546 4.971834, 18.996667 5.250886, 18.842221 5.41577, 18.856598 5.467162, 18.977188 5.528161, 19.11597 5.55445, 19.296153 5.52765, 19.57563 5.885911, 19.483152 6.177437, 19.557414 6.249171, 19.635434 6.708437, 20.02157 6.714001, 20.028388 6.558196, 20.174998 6.241253, 20.458091 6.253689, 20.621603 6.729513, 20.620393 6.839913, 20.762674 6.931398, 20.94569 6.93377, 21.147206 6.928947, 21.313003 6.938177, 21.248694 6.841382, 21.306712 6.37493, 21.299317 6.298949, 21.32155 5.992802, 21.412089 5.890303, 21.554039 5.768618))"
    )

    # MultiPoint
    params = {
        OptimalAlphaShapes.INPUT: str(multipoint),
        OptimalAlphaShapes.ALLOW_HOLES: False,
        OptimalAlphaShapes.NB_COMPONENTS: 1,
        OptimalAlphaShapes.OUTPUT: "TEMPORARY_OUTPUT",
    }

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:OptimalAlphaShapes")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[OptimalAlphaShapes.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 7

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "Polygon ((10.048373 8.775783, 10.236086 8.912395, 10.219867 8.739254, 9.863489 8.659003, 9.736457 8.524994, 9.672148 8.428198, 9.489574 8.352612, 9.197931 8.280721, 9.045057 8.31633, 9.043847 8.426729, 9.04785 8.56055, 9.074861 8.913475, 9.433219 8.900827, 9.645004 8.851581, 10.048373 8.775783))"
    )

    assert (
        output.getFeature(3).geometry().asWkt(precision=6)
        == "Polygon ((15.265797 -3.199598, 15.343817 -2.740333, 15.54116 -2.118267, 15.536461 -1.714578, 15.443983 -1.423052, 15.757877 -1.411804, 15.847853 -1.390067, 16.418923 -1.346801, 16.776692 -1.219596, 16.866869 -1.155022, 17.267543 -1.22556, 17.260148 -1.30154, 16.784872 -1.344837, 16.704872 -1.358299, 16.313142 -1.39912, 15.945161 -1.56438, 15.729954 -2.734768, 16.692486 -3.248296, 16.878004 -3.302485, 17.029933 -3.455967, 17.120472 -3.558467, 17.262422 -3.680152, 17.090163 -3.709584, 16.515334 -3.78962, 15.787284 -3.585367, 15.265797 -3.199598))"
    )

    assert (
        output.getFeature(5).geometry().asWkt(precision=6)
        == "Polygon ((2.238998 0.886052, 2.39852 0.636522, 1.991951 -0.70182, 2.099025 -0.78006, 2.256488 -0.856172, 2.371149 -0.987508, 2.07961 -1.209789, 1.07593 -1.200842, 0.568473 -1.124922, 0.54608 -0.98996, 0.522711 -0.56574, 0.552327 -0.103246, 0.780921 0.259612, 0.874618 0.617942, 1.280728 0.796594, 1.804381 0.866206, 1.985343 0.853292, 2.238998 0.886052))"
    )
