from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.difference import Difference
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_difference():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygone1 = f"{bdd}|layername=polygone"
    polygone2 = f"{bdd}|layername=polygone2"

    params = {
        Difference.INPUT: str(polygone1),
        Difference.OVERLAY: str(polygone2),
        Difference.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:Difference")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Difference.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 2
    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPolygon (((8.016334 3.759178, 7.807435 1.575227, 5.604493 1.233391, 5.452566 3.607251, 8.016334 3.759178)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=6)
        == "MultiPolygon (((0.528854 -0.188552, 0.384349 -1.163963, 1.722487 -1.240868, 1.73399 -0.953293, 1.992475 -0.624312, 0.789412 -0.607001, 0.887833 -0.104536, 0.528854 -0.188552)),((1.811997 0.886669, 1.646077 0.55483, 2.80207 0.430973, 2.827387 0.886669, 1.811997 0.886669)))"
    )

    # Linestring / Polygone
    linestring = f"{bdd}|layername=linestring"

    params = {
        Difference.INPUT: str(linestring),
        Difference.OVERLAY: str(polygone1),
        Difference.OUTPUT: "TEMPORARY_OUTPUT",
    }

    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Difference.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 1
    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiLineString ((1.04923 -1.202175, 0.72167 -1.513519),(2.224926 -0.084682, 1.66219 -0.619559))"
    )

    # Point / Polygone
    point = f"{bdd}|layername=point"

    params = {
        Difference.INPUT: str(point),
        Difference.OVERLAY: str(polygone1),
        Difference.OUTPUT: "TEMPORARY_OUTPUT",
    }

    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Difference.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 1
    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPoint ((5.013091 6.007062),(6.456812 5.367062),(3.762859 5.649852),(4.432626 6.736364),(5.12353 5.634388),(6.397277 6.825666))"
    )
