from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.line_sub_string import LineSubString
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_line_sub_string():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    linestring = f"{bdd}|layername=linestring"

    params = {
        LineSubString.INPUT: str(linestring),
        LineSubString.START: 0.1,
        LineSubString.END: 0.9,
        LineSubString.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:LineSubString")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[LineSubString.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 1

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "LineString (2.0746 -0.227565, 0.871996 -1.370635)"
    )
