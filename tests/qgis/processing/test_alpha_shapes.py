from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.alpha_shapes import AlphaShapes
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_alpha_shapes():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    multipoint = f"{bdd}|layername=random_points_multipoints"
    point = f"{bdd}|layername=random_points"

    # Point
    params = {
        AlphaShapes.INPUT: str(point),
        AlphaShapes.ALPHA: 80,
        AlphaShapes.ALLOW_HOLES: False,
        AlphaShapes.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:AlphaShapes")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[AlphaShapes.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 1

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "Polygon ((18.842221 5.41577, 18.856598 5.467162, 19.635434 6.708437, 20.762674 6.931398, 20.94569 6.93377, 21.313003 6.938177, 21.554039 5.768618, 21.560184 5.477298, 21.543965 5.304157, 21.056125 5.120547, 20.006931 4.935986, 19.382031 4.86593, 19.192546 4.971834, 18.842221 5.41577))"
    )

    # MultiPoint
    params = {
        AlphaShapes.INPUT: str(multipoint),
        AlphaShapes.ALPHA: 80,
        AlphaShapes.ALLOW_HOLES: False,
        AlphaShapes.OUTPUT: "TEMPORARY_OUTPUT",
    }

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:AlphaShapes")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[AlphaShapes.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 7

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "Polygon ((9.433219 8.900827, 10.236086 8.912395, 10.219867 8.739254, 9.672148 8.428198, 9.489574 8.352612, 9.197931 8.280721, 9.045057 8.31633, 9.043847 8.426729, 9.04785 8.56055, 9.074861 8.913475, 9.433219 8.900827))"
    )

    assert (
        output.getFeature(3).geometry().asWkt(precision=6)
        == "Polygon ((15.265797 -3.199598, 15.343817 -2.740333, 15.443983 -1.423052, 16.866869 -1.155022, 17.267543 -1.22556, 17.260148 -1.30154, 17.262422 -3.680152, 17.090163 -3.709584, 16.515334 -3.78962, 15.787284 -3.585367, 15.265797 -3.199598))"
    )

    assert (
        output.getFeature(5).geometry().asWkt(precision=6)
        == "Polygon ((2.238998 0.886052, 2.39852 0.636522, 2.371149 -0.987508, 2.07961 -1.209789, 1.07593 -1.200842, 0.568473 -1.124922, 0.54608 -0.98996, 0.522711 -0.56574, 0.552327 -0.103246, 0.874618 0.617942, 1.280728 0.796594, 1.804381 0.866206, 2.238998 0.886052))"
    )
