from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.extrude import Extrude
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


@pytest.mark.skip(
    reason="For the moment, tests are run on QGIS version 3.34, which does not support PolyhedralSurfaceZ."
)
def test_extrude():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygon = f"{bdd}|layername=polygone"

    params = {
        Extrude.INPUT: str(polygon),
        Extrude.FORCE_SINGLEPART: True,
        Extrude.EXTRUDE_X: 0,
        Extrude.EXTRUDE_X: 0,
        Extrude.EXTRUDE_X: 1,
        Extrude.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = QgsApplication.instance().processingRegistry().algorithmById("sfcgal:Extrude")
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Extrude.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 2

    excepted_wkt = (
        "PolyhedralSurfaceZ "
        "(((8.02 3.76 0, 7.81 1.58 0, 5.6 1.23 0, 5.45 3.61 0, 8.02 3.76 0)),"
        "((8.02 3.76 1, 5.45 3.61 1, 5.6 1.23 1, 7.81 1.58 1, 8.02 3.76 1)),"
        "((8.02 3.76 0, 8.02 3.76 1, 7.81 1.58 1, 7.81 1.58 0, 8.02 3.76 0)),"
        "((7.81 1.58 0, 7.81 1.58 1, 5.6 1.23 1, 5.6 1.23 0, 7.81 1.58 0)),"
        "((5.6 1.23 0, 5.6 1.23 1, 5.45 3.61 1, 5.45 3.61 0, 5.6 1.23 0)),"
        "((5.45 3.61 0, 5.45 3.61 1, 8.02 3.76 1, 8.02 3.76 0, 5.45 3.61 0)))"
    )
    assert output.getFeature(1).geometry().asWkt(precision=2) == excepted_wkt
