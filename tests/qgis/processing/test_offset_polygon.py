from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.offset_polygon import OffsetPolygon
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_offset_polygon():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygone = f"{bdd}|layername=polygone"

    params = {
        OffsetPolygon.INPUT: str(polygone),
        OffsetPolygon.DISTANCE: 1,
        OffsetPolygon.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:OffsetPolygon")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[OffsetPolygon.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 2

    assert (
        output.getFeature(1).geometry().asWkt(precision=4)
        == "MultiPolygon (((9.0163 3.7592, 8.8839 4.2564, 8.7515 4.437, 8.6192 4.5571, 8.4868 4.6416, 8.3544 4.7003, 8.222 4.7378, 8.0896 4.7565, 7.9572 4.7574, 6.5995 4.677, 5.3934 4.6055, 5.2758 4.5915, 5.1582 4.5629, 5.0406 4.5184, 4.923 4.4555, 4.8054 4.3696, 4.6878 4.2515, 4.5702 4.0778, 4.4526 3.6073, 4.4528 3.5847, 4.4531 3.5753, 4.4533 3.5681, 4.4536 3.5621, 4.4538 3.5567, 4.4541 3.5519, 4.4544 3.5475, 4.4546 3.5434, 4.5257 2.4323, 4.6065 1.1695, 4.7504 0.7132, 4.8944 0.5293, 5.0383 0.4091, 5.1822 0.3269, 5.3261 0.2729, 5.47 0.2425, 5.6139 0.2334, 5.7578 0.2452, 6.6904 0.3899, 7.9608 0.5871, 8.066 0.6092, 8.1713 0.6438, 8.2766 0.6921, 8.3818 0.7567, 8.4871 0.8417, 8.5924 0.9556, 8.6976 1.1196, 8.8029 1.48, 8.8974 2.468, 9.0118 3.664, 9.0124 3.6701, 9.0129 3.6767, 9.0135 3.6839, 9.0141 3.6918, 9.0146 3.7008, 9.0152 3.7115, 9.0158 3.7255, 9.0163 3.7592)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=4)
        == "MultiPolygon (((-0.6049 -1.0174, -0.6062 -1.0268, -0.6076 -1.037, -0.6089 -1.048, -0.6103 -1.0602, -0.6116 -1.0741, -0.613 -1.0905, -0.6143 -1.112, -0.6157 -1.164, -0.4978 -1.6349, -0.38 -1.8088, -0.2622 -1.9269, -0.1443 -2.0128, -0.0265 -2.0757, 0.0913 -2.1201, 0.2091 -2.1485, 0.327 -2.1623, 1.4914 -2.2292, 2.5295 -2.2889, 2.6617 -2.2877, 2.7938 -2.2689, 2.926 -2.2313, 3.0582 -2.1725, 3.1904 -2.0879, 3.3225 -1.9679, 3.4547 -1.7874, 3.5869 -1.2905, 3.5867 -1.2702, 3.5865 -1.2617, 3.5863 -1.2553, 3.5861 -1.2498, 3.5858 -1.245, 3.5856 -1.2407, 3.5854 -1.2367, 3.5852 -1.2329, 3.5651 -0.8849, 3.5472 -0.5747, 3.5401 -0.5, 3.533 -0.4545, 3.5258 -0.4188, 3.5187 -0.3884, 3.5116 -0.3616, 3.5044 -0.3374, 3.4973 -0.3152, 3.4902 -0.2946, 3.529 -0.2558, 3.5678 -0.2122, 3.6066 -0.163, 3.6453 -0.1065, 3.6841 -0.0401, 3.7229 0.0411, 3.7617 0.1498, 3.8005 0.3755, 3.8125 0.5907, 3.8258 0.8312, 3.826 0.8348, 3.8262 0.8387, 3.8264 0.8428, 3.8266 0.8475, 3.8268 0.8527, 3.827 0.8589, 3.8272 0.8671, 3.8274 0.8867, 3.7024 1.3708, 3.5774 1.5481, 3.4524 1.6673, 3.3274 1.7527, 3.2024 1.8137, 3.0774 1.8549, 2.9524 1.8788, 2.8274 1.8867, 0.6881 1.8867, 0.5645 1.879, 0.4408 1.8556, 0.3172 1.8153, 0.1935 1.7558, 0.0699 1.6726, -0.0538 1.5572, -0.1774 1.3875, -0.3011 1.0332, -0.4752 -0.1424, -0.6049 -1.0174)))"
    )
