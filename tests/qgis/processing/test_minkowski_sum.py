from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.minkowski_sum import Minkowski_sum
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_minkowski_sum():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    linestring = f"{bdd}|layername=linestring"
    polygone = f"{bdd}|layername=single_parts"

    params = {
        Minkowski_sum.INPUT_A: str(linestring),
        Minkowski_sum.INPUT_B: str(polygone),
        Minkowski_sum.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:Minkowski_sum")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Minkowski_sum.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 6

    assert (
        output.getFeature(1).geometry().asWkt(precision=3)
        == "MultiPolygon (((7.677 3.523, 6.174 2.094, 6.326 -0.28, 8.529 0.062, 10.032 1.491, 10.241 3.674, 7.677 3.523)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=3)
        == "MultiPolygon (((1.106 -2.677, 3.309 -2.804, 4.812 -1.375, 4.774 -0.717, 3.921 -0.705, 5.027 0.346, 5.052 0.802, 2.913 0.802, 1.41 -0.627, 1.106 -2.677)))"
    )
    assert (
        output.getFeature(3).geometry().asWkt(precision=3)
        == "MultiPolygon (((7.463 -3.951, 5.859 -3.781, 5.87 -3.595, 6.243 -3.56, 7.746 -2.131, 7.475 -1.725, 5.554 -1.917, 4.051 -3.346, 3.825 -5.73, 6.265 -5.967, 7.769 -4.538, 7.463 -3.951)))"
    )
    assert (
        output.getFeature(4).geometry().asWkt(precision=3)
        == "MultiPolygon (((-0.599 -5.289, 1.785 -5.346, 3.288 -3.917, 3.514 -2.64, 1.164 -2.55, -0.34 -3.978, -0.599 -5.289)))"
    )
    assert (
        output.getFeature(5).geometry().asWkt(precision=3)
        == "MultiPolygon (((9.74 6.672, 10.893 7.033, 12.396 8.462, 12.531 8.88, 11.243 8.869, 9.74 7.44, 9.74 6.672)))"
    )
    assert (
        output.getFeature(6).geometry().asWkt(precision=3)
        == "MultiPolygon (((13.176 8.926, 11.672 7.497, 11.729 7.124, 12.283 6.548, 13.119 6.74, 14.622 8.168, 14.678 8.926, 13.176 8.926)))"
    )
