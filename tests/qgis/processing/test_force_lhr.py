from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.force_lhr import Force_LHR
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_force_lhr():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygon = f"{bdd}|layername=polygone"

    params = {
        Force_LHR.INPUT: str(polygon),
        Force_LHR.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance().processingRegistry().algorithmById("sfcgal:Force_LHR")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Force_LHR.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 2
    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPolygon (((8.016334 3.759178, 5.452566 3.607251, 5.604493 1.233391, 7.807435 1.575227, 8.016334 3.759178)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=6)
        == "MultiPolygon (((2.827387 0.886669, 0.688146 0.886669, 0.384349 -1.163963, 2.58688 -1.290546, 2.548906 -0.632318, 0.789412 -0.607001, 1.029918 0.620847, 2.80207 0.430973, 2.827387 0.886669)))"
    )
