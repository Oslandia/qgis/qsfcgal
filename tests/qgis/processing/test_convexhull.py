from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.convexhull import Convexhull
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_convex_hull():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    point = f"{bdd}|layername=point"

    params = {
        Convexhull.INPUT: str(point),
        Convexhull.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:ConvexHull")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Convexhull.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 1

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "Polygon ((3.762859 5.649852, 6.456812 5.367062, 6.397277 6.825666, 4.432626 6.736364, 3.762859 5.649852))"
    )
