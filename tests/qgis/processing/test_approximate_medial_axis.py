from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.approximate_medial_axis import ApproximateMedialAxis
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_approximate_medial_axis():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygone = f"{bdd}|layername=polygone"

    params = {
        ApproximateMedialAxis.INPUT: str(polygone),
        ApproximateMedialAxis.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:ApproximateMedialAxis")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[ApproximateMedialAxis.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 2

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiLineString ((6.768149 2.555345, 6.656193 2.543368))"
    )

    assert (
        output.getFeature(2).geometry().asWkt(precision=6)
        == "MultiLineString ((0.927615 0.759602, 0.851693 0.697108),(0.927615 0.759602, 2.5987 0.670335),(0.601257 -0.75962, 0.715733 -0.894252),(0.601257 -0.75962, 0.851693 0.697108),(0.715733 -0.894252, 2.245449 -0.949189))"
    )
