from qgis.core import QgsProcessingFeedback


class ConsoleProcessingFeedback(QgsProcessingFeedback):
    def setProgressText(self, text: str):
        print(text)

    def pushWarning(self, warning: str):
        print(warning)

    def pushInfo(self, info: str):
        print(info)

    def pushCommandInfo(self, info: str):
        print(info)

    def pushDebugInfo(self, info):
        print(info)

    def pushConsoleInfo(self, info: str):
        print(info)

    def reportError(self, error: str, fatalError=False):
        print(error)
