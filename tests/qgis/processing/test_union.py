from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.union import Union
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_cg_union():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygone1 = f"{bdd}|layername=polygone"
    polygone2 = f"{bdd}|layername=polygone2"

    params = {
        Union.INPUT: str(polygone1),
        Union.OVERLAY: str(polygone2),
        Union.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = QgsApplication.instance().processingRegistry().algorithmById("sfcgal:Union")
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Union.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 2

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPolygon (((8.016334 3.759178, 7.807435 1.575227, 5.604493 1.233391, 5.452566 3.607251, 8.016334 3.759178)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=6)
        == "Polygon ((1.722487 -1.240868, 1.721429 -1.267305, 2.091963 -1.480833, 2.933515 -1.380349, 3.662023 -0.859089, 3.58666 -0.099181, 2.487618 -0.413193, 2.01032 -0.6016, 1.992475 -0.624312, 0.789412 -0.607001, 0.887833 -0.104536, 1.179344 -0.03631, 1.477019 0.216714, 1.646077 0.55483, 2.80207 0.430973, 2.827387 0.886669, 1.811997 0.886669, 1.849112 0.9609, 1.983065 1.719969, 1.060275 1.675318, -0.681121 0.365551, -0.472749 -0.095845, 0.47981 -0.200031, 0.528854 -0.188552, 0.384349 -1.163963, 1.722487 -1.240868))"
    )
