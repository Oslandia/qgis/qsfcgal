from pathlib import Path

import pytest
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
    QgsWkbTypes,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.intersection_3d import Intersection_3D
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


@pytest.mark.skipif(
    Qgis.QGIS_VERSION_INT <= 34000,
    reason="For the moment, tests are run on QGIS version 3.34, which does not support PolyhedralSurfaceZ.",
)
def test_intersection_3d():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"

    # Two polydrane with intersection
    params = {
        Intersection_3D.INPUT: f"{bdd}|layername=polyhedral1",
        Intersection_3D.OVERLAY: f"{bdd}|layername=polyhedral2",
        Intersection_3D.FORCE_SOLID: True,
        Intersection_3D.OUTPUT: "TEMPORARY_OUTPUT",
    }
    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:Intersection_3D")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Intersection_3D.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 1

    layer_resultat = QgsVectorLayer(str(bdd) + "|layername=resultat", "resultat", "ogr")

    feature_resultat = layer_resultat.getFeature(1).geometry()
    feature_output = output.getFeature(1).geometry()

    assert feature_resultat == feature_output

    assert output.getFeature(1).geometry().wkbType() == 1015

    # Two polydrane with no intersection
    params = {
        Intersection_3D.INPUT: f"{bdd}|layername=polyhedrale1",
        Intersection_3D.OVERLAY: f"{bdd}|layername=polyhedrale99",
        Intersection_3D.FORCE_SOLID: True,
        Intersection_3D.OUTPUT: "TEMPORARY_OUTPUT",
    }
    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:Intersection_3D")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Intersection_3D.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 0
