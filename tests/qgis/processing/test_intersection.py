from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.intersection import Intersection
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_intersection():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygone1 = f"{bdd}|layername=polygone"
    polygone2 = f"{bdd}|layername=polygone2"

    params = {
        Intersection.INPUT: str(polygone1),
        Intersection.OVERLAY: str(polygone2),
        Intersection.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance()
        .processingRegistry()
        .algorithmById("sfcgal:Intersection")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Intersection.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 1

    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPolygon (((1.992475 -0.624312, 1.73399 -0.953293, 1.722487 -1.240868, 2.58688 -1.290546, 2.548906 -0.632318, 1.992475 -0.624312)),((0.528854 -0.188552, 0.887833 -0.104536, 1.029918 0.620847, 1.646077 0.55483, 1.811997 0.886669, 0.688146 0.886669, 0.528854 -0.188552)))"
    )

    ## Polygone / Point without intersection
    polygone1 = f"{bdd}|layername=polygone"
    point = f"{bdd}|layername=point"

    params = {
        Intersection.INPUT: str(polygone1),
        Intersection.OVERLAY: str(point),
        Intersection.OUTPUT: "TEMPORARY_OUTPUT",
    }

    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Intersection.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 0
