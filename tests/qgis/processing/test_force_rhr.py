from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.force_rhr import Force_RHR
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_force_rhr():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    polygon = f"{bdd}|layername=polygone"

    params = {
        Force_RHR.INPUT: str(polygon),
        Force_RHR.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance().processingRegistry().algorithmById("sfcgal:Force_RHR")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Force_RHR.OUTPUT])
    assert output is not None
    assert output.isValid()
    assert output.featureCount() == 2
    print(output.getFeature(1).geometry().asWkt(precision=6))
    print(output.getFeature(2).geometry().asWkt(precision=6))
    assert (
        output.getFeature(1).geometry().asWkt(precision=6)
        == "MultiPolygon (((8.016334 3.759178, 7.807435 1.575227, 5.604493 1.233391, 5.452566 3.607251, 8.016334 3.759178)))"
    )
    assert (
        output.getFeature(2).geometry().asWkt(precision=6)
        == "MultiPolygon (((2.827387 0.886669, 2.80207 0.430973, 1.029918 0.620847, 0.789412 -0.607001, 2.548906 -0.632318, 2.58688 -1.290546, 0.384349 -1.163963, 0.688146 0.886669, 2.827387 0.886669)))"
    )
