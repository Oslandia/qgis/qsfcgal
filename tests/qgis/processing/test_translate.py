from pathlib import Path

import pytest
from qgis.core import (
    QgsApplication,
    QgsExpression,
    QgsProcessingContext,
    QgsProcessingRegistry,
    QgsVectorLayer,
)
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider
from qsfcgal.processing.translate import Translate
from tests.qgis.processing.console_processing_feedback import ConsoleProcessingFeedback

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_translate():
    bdd = Path(__file__).parent / "fixtures" / "tests_data.gpkg"
    line = f"{bdd}|layername=linestring"

    params = {
        Translate.INPUT: str(line),
        Translate.TRANSLATE_X: 1,
        Translate.TRANSLATE_Y: 2,
        Translate.OUTPUT: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()

    alg = (
        QgsApplication.instance().processingRegistry().algorithmById("sfcgal:Translate")
    )
    result, success = alg.run(params, context, feedback)

    assert success
    output = context.getMapLayer(result[Translate.OUTPUT])
    assert output is not None
    assert output.isValid()

    assert output.featureCount() == 1
    excepted_wkt = "MultiLineString ((3.22 1.92, 1.72 0.49))"
    assert output.getFeature(1).geometry().asWkt(precision=2) == excepted_wkt
