from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_alpha_shapes():
    from qsfcgal.expression.cg_alpha_shapes import cg_alpha_shapes

    QgsExpression.registerFunction(cg_alpha_shapes)

    exp = QgsExpression(
        "cg_alpha_shapes(geom_from_wkt('MultiPoint ((3.3 5.6), (3.4 5.3), (3.6 5.7), (3.6 5.3), (3.4 5.5), (3.8 5.5))'), 80, False)"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=2)
        == "Polygon ((3.6 5.7, 3.8 5.5, 3.6 5.3, 3.4 5.3, 3.3 5.6, 3.6 5.7))"
    )

    exp = QgsExpression(
        "cg_alpha_shapes(geom_from_wkt('LINESTRING(3 4,10 50,20 25)'), 80, False)"
    )
    result = exp.evaluate()
    assert result is None
