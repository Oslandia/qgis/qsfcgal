from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_minkowski_sum():
    from qsfcgal.expression.cg_minkowski_sum import cg_minkowski_sum

    QgsExpression.registerFunction(cg_minkowski_sum)
    exp = QgsExpression(
        "cg_minkowski_sum( geom_from_wkt('MultiLineString ((5.233 2.246, 7.867 1.278))'), geom_from_wkt('Polygon ((8.016 3.759, 7.807 1.575, 5.604 1.233, 5.453 3.607, 8.016 3.759))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=3)
        == "MultiPolygon (((13.249 6.005, 10.686 5.853, 10.837 3.479, 13.471 2.511, 15.674 2.853, 15.883 5.037, 13.249 6.005)))"
    )
