from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_force_lhr():
    from qsfcgal.expression.cg_force_lhr import cg_force_lhr

    QgsExpression.registerFunction(cg_force_lhr)
    exp = QgsExpression(
        "cg_force_lhr(geom_from_wkt('MultiPolygon (((8.016 3.759, 7.807 1.575, 5.604 1.233, 5.453 3.607, 8.016 3.759)))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=3)
        == "MultiPolygon (((8.016 3.759, 5.453 3.607, 5.604 1.233, 7.807 1.575, 8.016 3.759)))"
    )
