from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_force_rhr():
    from qsfcgal.expression.cg_force_rhr import cg_force_rhr

    QgsExpression.registerFunction(cg_force_rhr)
    exp = QgsExpression(
        "cg_force_rhr(geom_from_wkt('MultiPolygon (((8.239 10.909,7.222 10.649,7.391 10.197,8.329 9.993,8.894 10.219,8.883 10.784,8.239 10.909)))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=3)
        == "MultiPolygon (((8.239 10.909, 8.883 10.784, 8.894 10.219, 8.329 9.993, 7.391 10.197, 7.222 10.649, 8.239 10.909)))"
    )
