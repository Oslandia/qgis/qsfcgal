from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_difference():
    from qsfcgal.expression.cg_difference import cg_difference

    QgsExpression.registerFunction(cg_difference)
    exp = QgsExpression(
        "cg_difference(geom_from_wkt('MULTIPOLYGON (((8.016 3.759, 5.453 3.607, 5.604 1.233, 7.807 1.575, 8.016 3.759)))'),geom_from_wkt('MULTIPOLYGON (((5.016 2.145, 8.320 2.525, 8.301 1.195, 5.244 0.949, 5.016 2.145))') )"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=3)
        == "Polygon ((5.453 3.607, 5.542 2.206, 7.893 2.476, 8.016 3.759, 5.453 3.607))"
    )
