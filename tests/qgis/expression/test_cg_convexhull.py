from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_convexhull():
    from qsfcgal.expression.cg_convexhull import cg_convexhull

    QgsExpression.registerFunction(cg_convexhull)
    exp = QgsExpression(
        "cg_convexhull(geom_from_wkt( 'MultiPoint ((5.01309128 6.00706177),"
        "(6.45681221 5.36706177),(3.76285872 5.64985247),(4.43262616 6.7363641),"
        "(5.12353046 5.63438799),(6.39727733 6.82566643))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=6)
        == "Polygon ((3.762859 5.649852, 6.456812 5.367062, 6.397277 6.825666, 4.432626 6.736364, 3.762859 5.649852))"
    )
