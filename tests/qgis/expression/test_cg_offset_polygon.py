from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_offset_polygon():
    from qsfcgal.expression.cg_offset_polygon import cg_offset_polygon

    QgsExpression.registerFunction(cg_offset_polygon)
    exp = QgsExpression(
        "cg_offset_polygon(geom_from_wkt( 'MultiPoint ((5.01309128 6.00706177),"
        "(6.45681221 5.36706177),(3.76285872 5.64985247),(4.43262616 6.7363641),"
        "(5.12353046 5.63438799),(6.39727733 6.82566643))'), 1)"
    )
    result = exp.evaluate()
    print(result.asWkt(precision=6))
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=6)
        == "MultiPolygon (((7.456812 5.367062, 7.413463 5.658299, 7.370114 5.774345, 7.326765 5.860197, 7.283416 5.929846, 7.240067 5.988763, 7.196718 6.039772, 7.153369 6.084564, 7.11002 6.124241, 7.145927 6.162701, 7.181834 6.20561, 7.217741 6.253968, 7.253649 6.309306, 7.289556 6.374181, 7.325463 6.453549, 7.36137 6.560101, 7.397277 6.825666, 7.148455 7.485767, 6.899632 7.690328, 6.65081 7.792993, 6.401988 7.825655, 6.153165 7.795413, 5.904343 7.695733, 5.65552 7.496335, 5.406698 6.962604, 5.159939 7.42267, 4.91318 7.613329, 4.666421 7.70865, 4.419662 7.73628, 4.172903 7.702047, 3.926144 7.598615, 3.679385 7.394109, 3.432626 6.736364, 3.433844 6.687023, 3.435062 6.666607, 3.43628 6.650955, 3.437498 6.637773, 3.438716 6.626169, 3.439934 6.615688, 3.441152 6.606059, 3.44237 6.597105, 3.357431 6.56398, 3.272492 6.521369, 3.187553 6.467791, 3.102614 6.400903, 3.017676 6.316712, 2.932737 6.207434, 2.847798 6.053168, 2.762859 5.649852, 2.97186 5.038035, 3.180861 4.836662, 3.389861 4.72202, 3.598862 4.663392, 3.807863 4.650866, 4.016864 4.68265, 4.225865 4.763497, 4.434866 4.909308, 4.586259 4.790978, 4.737653 4.711838, 4.889046 4.662268, 5.040439 4.637846, 5.191833 4.636723, 5.343226 4.65882, 5.494619 4.705791, 5.646013 4.781738, 5.872363 4.555632, 6.098713 4.433378, 6.325063 4.375779, 6.551412 4.371546, 6.777762 4.419966, 7.004112 4.530125, 7.230462 4.733449, 7.456812 5.367062)))"
    )
