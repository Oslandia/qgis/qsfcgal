from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_union():
    from qsfcgal.expression.cg_union import cg_union

    QgsExpression.registerFunction(cg_union)
    exp = QgsExpression(
        " cg_union(geom_from_wkt('MultiPolygon (((2.827 0.887, 2.802 0.431, 1.03 0.621, 0.789 -0.607, 2.549 -0.632, 2.587 -1.291, 0.384 -1.164, 0.688 0.887, 2.827 0.887)))'),geom_from_wkt('MultiPolygon (((1.06 1.675, 1.983 1.72, 1.849 0.961, 1.477 0.217, 1.179 -0.036, 0.48 -0.2, -0.473 -0.096, -0.681 0.366, 1.06 1.675)))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(precision=3)
        == "Polygon ((2.587 -1.291, 2.549 -0.632, 0.789 -0.607, 0.888 -0.104, 1.179 -0.036, 1.477 0.217, 1.646 0.555, 2.802 0.431, 2.827 0.887, 1.812 0.887, 1.849 0.961, 1.983 1.72, 1.06 1.675, -0.681 0.366, -0.473 -0.096, 0.48 -0.2, 0.529 -0.189, 0.384 -1.164, 2.587 -1.291))"
    )
