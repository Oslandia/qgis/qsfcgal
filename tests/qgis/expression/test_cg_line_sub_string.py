from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    global provider
    provider = QsfcgalProvider()
    QgsApplication.instance().processingRegistry().addProvider(provider)


def test_cg_line_sub_string():
    from qsfcgal.expression.cg_line_sub_string import cg_line_sub_string

    QgsExpression.registerFunction(cg_line_sub_string)

    # Geom is not a line
    exp = QgsExpression("cg_line_sub_string(geom_from_wkt('POINT (0 1)'),0.1,0.9)")
    result = exp.evaluate()
    assert result is None

    # Geom is linestring
    exp = QgsExpression(
        "cg_line_sub_string(geom_from_wkt('LINESTRING (30 10, 10 30, 40 40)'),0.1,0.9)"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert (
        result.asWkt(6)
        == "LineString (25.763932 14.236068, 10 30, 34.316718 38.105573)"
    )

    # Geom is multilinestring
    exp = QgsExpression(
        "cg_line_sub_string(geom_from_wkt('MULTILINESTRING ((10 10, 20 20, 10 40), (40 40, 30 30, 40 20, 30 10))'),0.1,0.9)"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    print(result.asWkt(6))
    assert (
        result.asWkt(6)
        == "MultiLineString ((12.581139 12.581139, 20 20, 11.632456 36.735089),(37 37, 30 30, 40 20, 33 13))"
    )
