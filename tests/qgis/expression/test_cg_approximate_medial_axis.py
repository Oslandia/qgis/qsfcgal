from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsExpression, QgsGeometry
from qgis.testing import start_app

from qsfcgal.processing import QsfcgalProvider

provider = None


@pytest.fixture(autouse=True, scope="module")
def setup_qgis():
    start_app()
    if not "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]:
        global provider
        provider = QsfcgalProvider()
        QgsApplication.instance().processingRegistry().addProvider(provider)


def test_processing_registry():
    assert "sfcgal" in [
        elem.id() for elem in QgsApplication.processingRegistry().providers()
    ]


def test_cg_convexhull():
    from qsfcgal.expression.cg_approximate_medial_axis import cg_approximate_medial_axis

    QgsExpression.registerFunction(cg_approximate_medial_axis)
    exp = QgsExpression(
        "cg_approximate_medial_axis(geom_from_wkt('MultiPolygon (((2.82738680597299563 0.88666941849432757,"
        " 2.80207035027679296 0.43097321596268223, 1.02991845154261563 0.6208466336842009, "
        "0.78941212242869163 -0.60700146758162177, 2.54890579331476719 -0.63231792327782443, "
        "2.58688047685907119 -1.29054577137909021, 0.38434883128945119 -1.16396349289807777, "
        "0.68814629964388141 0.88666941849432757, 2.82738680597299563 0.88666941849432757)))'))"
    )
    result = exp.evaluate()
    assert isinstance(result, QgsGeometry)
    assert result.asWkt() == (
        "MultiLineString ((0.92761544292438147 0.75960217383278561, 0.85169310058546732 "
        "0.69710845072452376),(0.92761544292438147 0.75960217383278561, 2.598700447829005 "
        "0.67033521886697156),(0.60125651339677322 -0.75962011862290835, 0.71573334163888025 "
        "-0.89425203249408136),(0.60125651339677322 -0.75962011862290835, 0.85169310058546732 "
        "0.69710845072452376),(0.71573334163888025 -0.89425203249408136, 2.24544854349499312 "
        "-0.94918905156919664))"
    )
