<!--

Thanks for taking the time to fill out this bug report correctly.

Please report only issues related to the plugin.

-->

# Add new aglo

Add a new QFCGAL algorithm to the plugin

## Algo name

`<algo_name>`

## To-do list

### Processing

- [ ] Add new processing file in `qsfcgal/processing/<algo_name>.py`
- [ ] Adding the new algorithm to `loadAlgorithms()` method in the `provider.py` file

### Expression

- [ ] Add the new expression file to `qsfcgal/expression` and name it `cg_<algo_name>.py`
- [ ] Document expression in code for display in QGIS calculator
- [ ] Add the import of the new expression to `plugin_main.py` (init and unload)

### Tests

- [ ] Add a test file for processing in `tests/qgis/processing` and name it `test_<algo_name>.py`
- [ ] Add a test file for expression in `tests/qgis/expression` and name it `test_cg_<algo_name>.py`

### Documentation

- [ ] Create a `<algo_name>.md` file in `docs/QSFCGAL` and write the doc in it.
- [ ] Illustrate the algo with an image filed in `docs/img` and named `<algo_name>.png` then insert this image in `docs/QSFCGAL/<algo_name>.md`
- [ ] Update in docs `/QSFCGAL/algorithm.md`
- [ ] Add new documentation page to `docs/index.md`
- [ ] Update `readme.md` with the new algo

---

### Author's checklist

- [ ] I've read the contribution guidelines and affirm:
  - [ ] my branch has been created from main
  - [ ] if the code is not completely achieved, I apply the Draft/WIP prefix
  - [ ] the DOD is satisfied specially tests
- [ ] assign myself (the MR creator will automatically be assigned)
- [ ] the box _Delete source branch when merge request is accepted._ is checked

### Reviewer's checklist

- [ ] assign yourself as Reviewer and remember that others developers can comment too

<!-- GitLab quick actions -->
/assign me
